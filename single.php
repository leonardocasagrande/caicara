<? the_post();?> <?= get_header(); ?> <section class="banner-blog"><div class="banner"><h1>Blog</h1></div></section><section class="intro-section intro-single-blog container"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/blog-icon.png" alt=""> <span class="title col-lg-5 px-0"><?= the_title(); ?></span><div class="line"></div></section><section class="main-single-blog"><div class="top-150"><div class="img-single-blog" style="background: url(<?= the_post_thumbnail_url(); ?>);"></div><div class="container"> <?= the_content(); ?> </div></div></section><section class="intro-section intro-section-blog container pb-0"><span class="title col-8 px-0">Mais notícias</span><div class="line"></div></section><section class="blog-carousel d-lg-none"> <?= get_template_part('carousel-blog'); ?> </section><section class="blog-posts white-bg"><div class="container d-none d-lg-block"> <?php
    $argsBlogLg = array(
      'post_type' => 'post',
      'order' => 'ASC',
      'posts_per_page' => 4,


    );
    $counter = 0;
    $blog = new WP_Query($argsBlogLg);

    ?> <?php if ($blog->have_posts()) : while ($blog->have_posts()) :  $blog->the_post(); ?> <?php if ($counter == 0) : ?> <div class="col-12 px-0 d-flex"><div class="item col-lg-6 px-0" style="background: url(<?= the_post_thumbnail_url(); ?>)center center no-repeat;"><div class="container"><div class="text"><div class="title"><?= the_title(); ?></div><p><?= the_excerpt(); ?></p><a href="<?= the_permalink(); ?>" class="btn-cta col-5 px-0">Leia mais</a></div></div></div> <?php elseif ($counter == 1) : ?> <div class="col-lg-6 px-0"><div class="item medium-size" style="background: url(<?= the_post_thumbnail_url(); ?>)center center no-repeat;"><div class="container"><div class="text"><div class="title"><?= the_title(); ?></div><a href="<?= the_permalink(); ?>" class="btn-cta col-5 px-0">Leia mais</a></div></div></div><div class="d-lg-flex"> <?php elseif ($counter == 2) : ?> <div class="item low-size" style="background: url(<?= the_post_thumbnail_url(); ?>)center center no-repeat;"><div class="container"><div class="text"><div class="title"><?= the_title(); ?></div><a href="<?= the_permalink(); ?>" class="btn-cta col-8 px-0">Leia mais</a></div></div></div> <?php elseif ($counter == 3) : ?> <div class="item low-size" style="background: url(<?= the_post_thumbnail_url(); ?>)center center no-repeat;"><div class="container"><div class="text"><div class="title"><?= the_title(); ?></div><a href="<?= the_permalink(); ?>" class="btn-cta col-8 px-0">Leia mais</a></div></div></div></div></div></div> <?php $counter = -1;
            endif;
            $counter++;
          endwhile;
        endif;
    ?> </div></section> <?= get_template_part('nossas-marcas'); ?> <?= get_footer(); ?>