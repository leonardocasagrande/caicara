$(".menu-icon").on("click", function () {
  $(".top-nav").toggleClass("ativo");
});

$(".menu").on("click", function () {
  if ($(".top-nav").hasClass("ativo")) {
    $(".top-nav").toggleClass("ativo");
    $(".menu-btn")[0].checked = false;
  }
});

$(".top-button").on("click", function (e) {
  e.preventDefault();
  window.scrollTo(0, 0);
});

$(".btn-categoria").on("click", function () {
  this.children[1].children[1].classList.toggle("d-none");
  this.children[1].children[2].classList.toggle("d-block");
});

let containerFake = $(".fake-carousel-sobre")[0];

$(".controls-fake .left").on("click", function (e) {
  e.preventDefault();
  containerFake.scrollLeft -= 150;
});

$(".controls-fake .right").on("click", function (e) {
  e.preventDefault();
  containerFake.scrollLeft += 150;
});

$(function () {
  var sliderMotivos = tns({
    container: ".carousel-motivos",
    items: 1.2,
    slideBy: 1,
    controls: false,
    autoplayButtonOutput: false,
    nav: false,
    loop: false,
    edgePadding: 10,
    responsive: {
      765: {
        items: 2.2,
      },
      1200: {
        disable: true,
      },
    },
  });
});

$(function () {
  var sliderReceitasDestaque = tns({
    container: ".carousel-destaques-receitas",
    items: 1,
    slideBy: 1,
    controls: true,
    controlsText: [
      "<i class='fa fa-chevron-left'></i>",
      "<i class='fa fa-chevron-right'></i>",
    ],
    autoplayButtonOutput: false,
    nav: false,
    loop: false,
    edgePadding: 10,
    gutter: 10,
    responsive: {
      765: {
        items: 2,
      },
      1200: {
        items: 3,
      },
    },
  });
});

$(function () {
  var sliderBlogReceitas = tns({
    container: ".carousel-blog-recipes",
    items: 1,
    slideBy: 1,
    controls: true,
    controlsText: [
      "<i class='fa fa-chevron-left'></i>",
      "<i class='fa fa-chevron-right'></i>",
    ],
    autoplayButtonOutput: false,
    nav: false,
  });
});

$(function () {
  var sliderMarcas = tns({
    container: ".carousel-marcas",
    items: 1,
    slideBy: 1,
    controls: true,
    controlsText: [
      "<i class='fa fa-chevron-left'></i>",
      "<i class='fa fa-chevron-right'></i>",
    ],
    autoplayButtonOutput: false,
    nav: false,
    responsive: {
      1200: {
        disable: true,
      },
    },
  });
});

$(function () {
  var sliderMaquinas1 = tns({
    container: ".red-box-carousel",
    items: 1,
    slideBy: 1,
    controls: false,
    autoplayButtonOutput: false,
    nav: true,
    navPosition: "bottom",
    responsive: {
      1200: {
        disable: true,
      },
    },
  });
});

$(function () {
  var sliderMaquinas2 = tns({
    container: ".carousel-maquinas",
    items: 1,
    slideBy: 1,
    loop: false,
    controls: true,
    controlsText: [
      "<i class='fa fa-chevron-left'></i>",
      "<i class='fa fa-chevron-right'></i>",
    ],
    autoplayButtonOutput: false,
    nav: false,
  });
});

$(function () {
  var sliderMaquinasHome = tns({
    container: ".carousel-maquinas-home",
    items: 1,
    slideBy: 1,
    loop: false,
    controls: true,
    controlsPosition: "bottom",
    controlsText: [
      "<i class='fa fa-chevron-left'></i>",
      "<i class='fa fa-chevron-right'></i>",
    ],
    autoplayButtonOutput: false,
    nav: false,
  });
});

// $(function () {

//   let positionMarca = 1;
//   const arrayMarcas = $('.carousel-marcas')[0].children;

//   $('.next-marca').on('click', function () {
//     arrayMarcas[7].classList.remove('no-filter');

//     arrayMarcas[positionMarca].classList.remove('no-filter');

//     positionMarca++;

//     arrayMarcas[positionMarca].classList.add('no-filter');

//     if (positionMarca >= 7) {
//       positionMarca = 0;
//     }

//   });
// });

//  POPULAR SELECT PRODUTOS FORM

$(function () {
  var url = document.location.origin ;
  var produtoSelect = $(".custom-produto")[0];

  $.ajax({
    url: `${url}/wp-json/wp/v2/produtos?orderby=title&order=asc&per_page=100&exclude=2030,2014,2032,2019,2042,2048,2044`,
    type: "GET",
    dataType: "json",
    statusCode: {
      200: function (data, status, xhr) {
        data.map((element) => {
          var input = document.createElement("option");

          input.text = `${element.title.rendered}  ${element.acf.embalagem} ${element.acf.peso}`;

          input.value = `${element.title.rendered}  ${element.acf.embalagem} ${element.acf.peso}`;

          produtoSelect.append(input);
        });
      },
    },
  });
});

// ----------------

function validateEmail(emailField) {
  var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

  if (reg.test(emailField.value) == false) {
    alert("Email Inválido");
    return false;
  }

  return true;
}

$(".e-mail").attr("onChange", "validateEmail(this)");

$(".phone").inputmask({
  placeholder: "",
  mask: function () {
    return ["(99) 9999-9999", "(99) 99999-9999"];
  },
});

$(".custom-cnpj").inputmask({
  placeholder: "",
  mask: function () {
    return "99.999.999/9999-99";
  },
});

//onde encontrar -sacForm

$(function () {
  let onde_encontrar_span = document.createElement("div");
  onde_encontrar_span.classList.add("onde-encontrar-span");
  $(".section-sac .lote").append(onde_encontrar_span);

  let span_red = document.createElement("span");
  span_red.innerHTML = "Onde encontrar?";
  onde_encontrar_span.append(span_red);

  let newContentDiv = document.createElement("div");
  let newContentDiv2 = document.createElement("div");

  newContentDiv.classList.add("span-hover");
  newContentDiv.append(newContentDiv2);

  $(".onde-encontrar-span").append(newContentDiv);

  if (window.innerWidth <= 800) {
    $(".onde-encontrar-span").on("click", function () {
      $(".span-hover").toggleClass("d-block");

      if ($(".span-hover").hasClass("d-block")) {
        $("html").on("click", function () {
          $(".span-hover").removeClass("d-block");
        });

        $(".onde-encontrar-span").on("click", function (event) {
          event.stopPropagation();
        });
      }
    });
  }
  // console.log($('.section-sac .lote'));
});

// RADIO BUTTONS WITH THUMBNAIL
$(function () {
  function radioThumb(arrayRadio) {
    arrayRadio.forEach((e, i) => {
      let label = arrayRadio[i].childNodes[0];

      let newBoxThumb = document.createElement("div");

      newBoxThumb.classList.add(`box-thumb`, `box-${i}`);

      label.insertBefore(newBoxThumb, label.childNodes[0]);
    });
  }

  let arrayTextura = $(".textura-cafe .wpcf7-radio")[0].childNodes;
  let arrayEmbalagem = $(".tipo-embalagem .wpcf7-radio")[0].childNodes;

  radioThumb(arrayTextura);
  radioThumb(arrayEmbalagem);

  let arrayLinha = $(".linha-cafe .wpcf7-radio")[0].childNodes;

  arrayLinha.forEach((e, i) => {
    if (i === 0) {
      let label = e.childNodes[0];

      label.childNodes[1].remove();

      let newText = document.createElement("pre");

      newText.innerHTML = `
    <b>Tradicional</b>

    <b>Composição</b>: blend arábica 
    + conilon
    <b>Aroma</b>: moderado
    <b>Acidez</b>: baixa
    <b>Amargor</b>: médio
    <b>Sabor</b>: característico
    <b>Corpo</b>: encorpado`;

      label.append(newText);
    }

    if (i === 1) {
      let label = e.childNodes[0];

      label.childNodes[1].remove();

      let newText = document.createElement("pre");

      newText.innerHTML = `
    <b>Superior</b>

    <b>Composição</b>: 100% arábica  
    <b>Aroma</b>: característico 
    <b>Acidez</b>: moderada
    <b>Amargor</b>: moderado
    <b>Sabor</b>: característico e
    equilibrado 
    <b>Corpo</b>: encorpado
    `;

      label.append(newText);
    }

    if (i === 2) {
      let label = e.childNodes[0];

      label.childNodes[1].remove();

      let newText = document.createElement("pre");

      newText.innerHTML = `
    <b>Gourmet</b>
    
    <b>Composição</b>: 100% arábica  
    <b>Aroma</b>: característico, marcante
    e intenso 
    <b>Acidez</b>: ponderada
    <b>Amargor</b>: nulo 
    <b>Sabor</b>: característico e equilibrado
    e limpo
    <b>Corpo</b>: encorpado, redondo
    e suave    
    `;

      label.append(newText);
    }
  });
});

// BOTOES VER MAIS

var url = document.location.origin ;
let pageNumber = 1;

// Produtos
$("#loadMoreProducts").on("click", function (e) {
  e.preventDefault();
  $("#loadMoreProducts ").addClass("btn-loading");

  pageNumber += 1;

  let responsivePostNumber = 4;

  let nossasMarcas = $("#loadMoreProducts").attr("data-marca");

  let postId = $("#loadMoreProducts").attr("data-post-id");

  if (window.screen.width > 400) {
    responsivePostNumber = 6;
  }

  $.ajax({
    url: `${url}/wp-json/wp/v2/produtos?per_page=${responsivePostNumber}&page=${pageNumber}&nossas_marcas=${nossasMarcas}&exclude=${postId}`,
    data: {},
    statusCode: {
      200: function (data, status, xhr) {
        const numPosts = xhr.getResponseHeader("X-WP-TotalPages");
        let content = `
        ${data
          .map((element) => {
            if (element.acf.embalagem && element.acf.peso) {
              return `
              <a href="${element.link}" class="item ">

              <img src="${element.featured_image_src}" alt="">
    
              <div class="line-red d-lg-none col-5 px-0"></div>
    
              <span class="name">${element.title.rendered}</span>

              <span class="name">Embalagem ${element.acf.embalagem}</span>
              <span class="name">${element.acf.peso}</span>
    
            </a>
              
              `;
            } else if (element.acf.peso) {
              return `
              <a href="${element.link}" class="item ">

              <img src="${element.featured_image_src}" alt="">
    
              <div class="line-red d-lg-none col-5 px-0"></div>
    
              <span class="name">${element.title.rendered}</span>

              <span class="name">${element.acf.peso}</span>
    
            </a>
              `;
            } else {
              return `
              <a href="${element.link}" class="item ">

              <img src="${element.featured_image_src}" alt="">
    
              <div class="line-red d-lg-none col-5 px-0"></div>
    
              <span class="name">${element.title.rendered}</span>

              </a>
              
              `;
            }
          })
          .join("")}`;
        if (pageNumber >= numPosts) {
          $("#loadMoreProducts").remove();
        }
        $(".product-box").append(content);
        $("#loadMoreProducts ").removeClass("btn-loading");
      },
    },
  });
});

// RECEITAS

function get_tax_name(element) {
  for (var i = 0; i <= element.categoria_receita_slug.length; i++) {
    if (
      element.categoria_receita[0] === element.categoria_receita_slug[i].term_id
    ) {
      return element.categoria_receita_slug[i].name;
    }
  }
}

var categoria_receita = "";
var categoria_url = "";

let pageNumber3 = 1;

$(`#showMoreRecipes-0`).on("click", function (e) {
  e.preventDefault();
  $(`#showMoreRecipes-0`).addClass("btn-loading");

  pageNumber3 += 1;

  let responsivePostNumber = 4;

  if (window.screen.width > 400) {
    responsivePostNumber = 6;
  }

  $.ajax({
    url: `${url}/wp-json/wp/v2/receita?per_page=${responsivePostNumber}&page=${pageNumber3}`,
    data: {},
    statusCode: {
      200: function (data, status, xhr) {
        const numPosts = xhr.getResponseHeader("X-WP-TotalPages");
        let content = `
    ${data
      .map(
        (element) => ` 
      <div class=" item col-lg-4 col-md-6">

      <div class="img-header" style="background: url(${
        element.featured_image_src
      })"></div>

      <div class="content">

      <span class="categoria">/${get_tax_name(element)}</span>

        <span class="title">${element.title.rendered}</span>

        <a href="${
          element.link
        }" class="btn-cta col-lg-6 col-8 px-0">Confira a receita</a>

      </div>

    </div>

`
      )
      .join("")}`;
        if (pageNumber3 >= numPosts) {
          $(`#showMoreRecipes-0`).attr("style", "display:none");
        }

        $(".receitas-response .active .flex-wrap").append(content);
        $(`#showMoreRecipes-0`).removeClass("btn-loading");
      },
    },
  });
});

$(".categorys .nav-item").on("click", function (e) {
  pageNumber = 1;

  setTimeout(function () {
    categoria_receita = $(".receitas-response .active").attr("data-tax");

    if (categoria_receita != 0) {
      $(`#showMoreRecipes-${categoria_receita}`).on("click", function (e) {
        e.preventDefault();
        $(`#showMoreRecipes-${categoria_receita}`).addClass("btn-loading");

        pageNumber += 1;

        let responsivePostNumber = 3;

        if (categoria_receita == 0) {
          categoria_url = "";
        } else {
          categoria_url = `&categoria_receita=${categoria_receita}`;
        }

        if (window.screen.width > 400) {
          responsivePostNumber = 6;
        }

        $.ajax({
          url: `${url}/wp-json/wp/v2/receita?per_page=${responsivePostNumber}&page=${pageNumber}${categoria_url}`,
          data: {},
          statusCode: {
            200: function (data, status, xhr) {
              const numPosts = xhr.getResponseHeader("X-WP-TotalPages");

              let content = `
    ${data
      .map(
        (element) => ` 
          <div class=" item col-lg-4 col-md-6">

          <div class="img-header" style="background: url(${
            element.featured_image_src
          })"></div>

          <div class="content">

          <span class="categoria">/${get_tax_name(element)}</span>

            <span class="title">${element.title.rendered}</span>

            <a href="${
              element.link
            }" class="btn-cta col-lg-6 col-8 px-0">Confira a receita</a>

          </div>

        </div>

    `
      )
      .join("")}`;

              if (pageNumber >= numPosts) {
                $(`#showMoreRecipes-${categoria_receita}`).attr(
                  "style",
                  "display:none"
                );
              }

              $(".receitas-response .active .flex-wrap").append(content);
              $(`#showMoreRecipes-${categoria_receita}`).removeClass(
                "btn-loading"
              );
            },

            400: function () {
              $(`#showMoreRecipes-${categoria_receita}`).attr(
                "style",
                "display:none"
              );
            },
          },
        });
      });
    }
  }, 200);
});

// Blog

let pageNumber2 = 1;

$("#showMoreBlog").on("click", function (e) {
  e.preventDefault();

  $("#showMoreBlog ").addClass("btn-loading");

  pageNumber2 += 1;

  let responsive = $("#showMoreBlog").attr("data-responsive");

  if (responsive === "true") {
    $.ajax({
      url: `${url}/wp-json/wp/v2/posts?per_page=2&page=${pageNumber2}`,
      data: {},
      statusCode: {
        200: function (data, status, xhr) {
          const numPosts = xhr.getResponseHeader("X-WP-TotalPages");
          let content = `
          ${data
            .map(
              (element) => ` 

              <div class="item low-size" style="background: url(${element.featured_image_src})center center no-repeat;">

                <div class="container">

                  <div class="text">
                    <span class="title">${element.title.rendered}</span>

                    <a href="${element.link}" class="btn-cta col-5 px-0">Leia mais</a>
                  </div>
                </div>

              </div>

        `
            )
            .join("")}`;
          if (pageNumber2 >= numPosts) {
            $("#showMoreBlog").remove();
          }
          $(".blog-posts .d-lg-none").append(content);
          $("#showMoreBlog ").removeClass("btn-loading");
        },
      },
    });
  } else {
    $.ajax({
      url: `${url}/wp-json/wp/v2/posts?per_page=8&page=${pageNumber2}`,
      data: {},
      statusCode: {
        200: function (data, status, xhr) {
          const numPosts = xhr.getResponseHeader("X-WP-TotalPages");

          data.map((element) => renderizaPost(element)).join("");

          if (pageNumber2 >= numPosts) {
            $("#showMoreBlog").remove();
          }
        },
      },
    });
  }
});

let counter = 0;
let classCounter = 0;
function renderizaPost(element) {
  let classArray = ["col-12", "px-0", "d-flex", "divMae"];
  let novaDiv = document.createElement("div");
  novaDiv.classList.add(...classArray);

  let classArray2 = ["d-flex", "referencia2"];
  let outraDiv = document.createElement("div");
  outraDiv.classList.add(...classArray2);

  if (counter === -1) {
    classCounter++;

    let classArray3 = ["col-12", "px-0", "d-flex", `divMae${classCounter}`];
    let maisUmaDiv = document.createElement("div");
    maisUmaDiv.classList.add(...classArray3);

    if (classCounter % 2 !== 0) {
      maisUmaDiv.classList.add("flex-row-reverse");
    }

    $(".blog-posts .d-lg-block").append(maisUmaDiv);
    counter++;
  }

  if (counter === 0) {
    let content = `


  <div class= "item col-lg-6 px-0" style = "background: url(${element.featured_image_src})center center no-repeat;" >

  <div class="container">

    <div class="text">
      <div class="title">${element.title.rendered}</div>
      <p></p>

      <a href="${element.link}" class="btn-cta col-5 px-0">Leia mais</a>
    </div>
  </div>

    </div>

    `;

    $(".blog-posts .d-lg-block").append(novaDiv);

    if (classCounter === 0) {
      $(".blog-posts .d-lg-block .divMae").append(content);
    } else {
      $(`.blog-posts .d-lg-block .divMae${classCounter} `).append(content);
    }

    $("#showMoreBlog ").removeClass("btn-loading");

    counter++;
  } else if (counter === 1) {
    let content = `

  <div class= "col-lg-6 referencia px-0" >
  <div class="item medium-size" style="background: url(${element.featured_image_src})center center no-repeat;">

    <div class="container">

      <div class="text">
        <div class="title">${element.title.rendered}</div>

        <a href="${element.link}" class="btn-cta col-5 px-0">Leia mais</a>
      </div>
    </div>
  </div>
      </div >
    </div >

    `;

    if (classCounter === 0) {
      $(".blog-posts .d-lg-block .divMae").append(content);
      $(".blog-posts .d-lg-block .divMae .referencia").append(outraDiv);
    } else {
      $(`.blog-posts .d-lg-block .divMae${classCounter} `).append(content);
      $(`.blog-posts .d-lg-block .divMae${classCounter} .referencia`).append(
        outraDiv
      );
    }
    $("#showMoreBlog ").removeClass("btn-loading");

    counter++;
  } else if (counter === 2) {
    let content = `

  <div class= "item low-size " style = "background: url(${element.featured_image_src})center center no-repeat;" >

  <div class="container">

    <div class="text">

      <div class="title">${element.title.rendered}</div>

      <a href="${element.link}" class="btn-cta col-8 px-0">Leia mais</a>

    </div>

  </div>
    
    </div>

    `;
    if (classCounter === 0) {
      $(".blog-posts .d-lg-block .divMae .referencia2").append(content);
    } else {
      $(`.blog-posts .d-lg-block .divMae${classCounter} .referencia2`).append(
        content
      );
    }

    $("#showMoreBlog ").removeClass("btn-loading");
    counter++;
  } else if (counter === 3) {
    let content = `

  <div class= "item low-size" style = "background: url(${element.featured_image_src})center center no-repeat;">

  <div class="container">

    <div class="text">

      <div class="title">${element.title.rendered}</div>

      <a href="${element.link}" class="btn-cta col-8 px-0">Leia mais</a>

    </div>

  </div>
    
  </div>

    `;

    if (classCounter === 0) {
      $(".blog-posts .d-lg-block .divMae .referencia2").append(content);
    } else {
      $(`.blog-posts .d-lg-block .divMae${classCounter} .referencia2`).append(
        content
      );
    }
    $("#showMoreBlog ").removeClass("btn-loading");

    counter = -1;
  }
}
