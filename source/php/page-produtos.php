<?php get_header() ?>
<section class="banner-nossas-marcas">
  <div class="banner">

    <h1><?= the_title(); ?></h1>

  </div>
</section>

<section class="intro-section container">

  <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/icon-nossas-marcas.png" alt="">

  <span class="title">Confira as nossas linhas</span>

  <div class="line "></div>


</section>
<?php
    $marcas = get_terms( array(
        'taxonomy'=> 'nossas_marcas',
        'orderby' => 'meta_value',
        'meta_key' => 'posicao'
    ));
    // var_dump($marcas);
    foreach ($marcas as $marca):
    
?>
<section class="nossos-produtos tax-black">

  <div class="container">

    <span class="title"><?= $marca->name ?></span>

    <div class="line col-7 col-lg-2 px-0"></div>

    <div class="product-box justify-content-start col-lg-12">

      <?php
      wp_reset_postdata();
      $argsProdutos =   array(
        'post_type' => 'produtos',
        'order' => 'ASC',
        'orderby' => 'menu_order',
        'posts_per_page' => -1,
        'tax_query' => array(
          array(
            'taxonomy' => 'nossas_marcas',
            'field' => 'term_id',
            'terms' => $marca->term_id,
          )

        )
      );

      $products = new WP_Query($argsProdutos);


      while ($products->have_posts()) : $products->the_post();


      ?>

        <a href="<?= the_permalink(); ?>" class="item ">

          <img src="<?= get_the_post_thumbnail_url(); ?>" alt="">

          <div class="line-red d-lg-none col-5 px-0"></div>

          <span class="name"><?= the_title(); ?></span>

          <?php if (get_field('embalagem')) :; ?>
            <span class="name">Embalagem <?= the_field('embalagem'); ?></span>
          <?php endif; ?>

          <span class="name"><?= the_field('peso'); ?></span>

        </a>

      <?php endwhile; wp_reset_postdata();?>



    </div>


  </div>
</section>
<?php endforeach; ?>
<?php get_footer() ?>