<?= get_header(); ?>


<?php
$paged = (isset($_GET['pagina']) ? $_GET['pagina'] : 1);
$id_tax = get_queried_object();
// var_dump($id_tax);
$numerowhats = get_field('atendimento_whats', 41);
$new_limit = 6;
if ($paged > 1) {
  $new_limit = 12;
}
$iphone = strpos($_SERVER['HTTP_USER_AGENT'], "iPhone");
$android = strpos($_SERVER['HTTP_USER_AGENT'], "Android");
$ipad = strpos($_SERVER['HTTP_USER_AGENT'], "iPad");
$berry = strpos($_SERVER['HTTP_USER_AGENT'], "BlackBerry");
$ipod = strpos($_SERVER['HTTP_USER_AGENT'], "iPod");

if ($iphone || $android || $ipad || $ipod || $berry == true) {
  $new_limit = 4;
}

if (is_tax('nossas_marcas', 'caicara')) : $tax_color = ' tax-red';
  $caicara = ' caicara';
elseif (is_tax('nossas_marcas', 'sao_joaquim')) : $tax_color = ' tax-yellow';
  $sj = ' sao-joaquim';
  $sj_custom = ' d-lg-flex';
elseif (is_tax('nossas_marcas', 'san_pietro')) : $tax_color = ' tax-brown';
elseif (is_tax('nossas_marcas', 'itatibense')) : $tax_color = ' tax-dbrown';
elseif (is_tax('nossas_marcas', 'bourbon')) : $tax_color = ' tax-black';
elseif (is_tax('nossas_marcas', 'negrao')) : $tax_color = ' tax-wine';
elseif (is_tax('nossas_marcas', 'campo_verde')) : $tax_color = ' tax-green';
endif;

if (is_tax('nossas_marcas', ['campo_verde', 'negrao', 'san_pietro', 'itatibense'])) : $d_none = ' d-none';
  $is_pages = ' d-block';
else : $is_pages = ' d-none';
endif;

?>


<section class="banner-nossas-marcas-single">
  <div class="banner" style="background: url(<?= the_field('banner_marca', $id_tax); ?>) center center no-repeat;">


  </div>
</section>

<?php if ($paged == 1) : ?>
  <section class="intro-section intro-section-tax mb-lg-5 <?= $tax_color; ?> container">

    <div class="box-marca-img col-lg-3">

      <img src="<?= the_field('logo_categoria', $id_tax); ?>" alt="">

    </div>

    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/cafe-icon.png" alt="">

    <span class="title col-lg-6"><?= the_field('titulo_marca', $id_tax); ?></span>

    <div class="line "></div>

    <div class="d-lg-flex <?= $caicara, $sj; ?> justify-content-between pt-lg-5">

      <div class="col-lg-5 position-relative">

        <?= the_field('texto_marca', $id_tax); ?>


        <div class="qualidade-certificada">

          <span class="title col-5 col-lg-3 px-0">Qualidade certificada</span>

          <div class="">

            <img class="col-5 col-lg-4 px-0" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/selo-abic.png" alt="">
            <img class="col-5 col-lg-4 px-0" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/selo-abic-2.png" alt="">

          </div>

        </div>
      </div>


      <div class="d-none d-lg-flex flex-column justify-content-between col-lg-6 px-0">
        <div class="img-caicara-wrapper">
          <div></div>
        </div>

        <div class="img-wrapper  ">
          <div style="background:url(<?= the_field('marca_lg', $id_tax); ?>) center center no-repeat"></div>
        </div>

        <div class="selo selo-caicara"></div>
        <div class="selo selo-sj"></div>
      </div>

    </div>

    <div class="sao-joaquim-custom <?= $sj_custom; ?>">

      <div class="time-line">
        <div class="y-1898"></div>
        <div class="y-1922"></div>
        <div class="y-1933"></div>
        <div class="y-1941"></div>
        <div class="y-2011"></div>
      </div>

      <div class="text col-lg-10">
        <span class="title">Linha do tempo Café São Joaquim</span>

        <ul>
        <?php while(have_rows('linha_do_tempo_sao_joaquim', $id_tax)): the_row(); ?>
          <li><span><?= get_sub_field('ano') ?></span> <?= get_sub_field('evento') ?></li>
        <?php endwhile; ?>
        </ul>
      </div>
    </div>


  </section>
<?php endif; ?>
<section class="nossos-produtos <?= $tax_color; ?>">

  <div class="container">

    <span id="anchor" class="title">Confira nossos produtos</span>

    <div class="line col-7 col-lg-2 px-0"></div>

    <div class="product-box col-lg-12">

      <?php
      $argsProdutos =   array(
        'post_type' => 'produtos',
        'order' => 'ASC',
        'posts_per_page' => $new_limit,
        'paged' => $paged,
        'tax_query' => array(
          array(
            'taxonomy' => 'nossas_marcas',
            'field' => 'term_id',
            'terms' => $id_tax->term_id,
          )

        )
      );

      $products = new WP_Query($argsProdutos);


      while ($products->have_posts()) : $products->the_post();


      ?>

        <a href="<?= the_permalink(); ?>" class="item ">

          <img src="<?= get_the_post_thumbnail_url(); ?>" alt="">

          <div class="line-red d-lg-none col-5 px-0"></div>

          <span class="name"><?= the_title(); ?></span>

          <?php if (get_field('embalagem')) :; ?>
            <span class="name">Embalagem <?= the_field('embalagem'); ?></span>
          <?php endif; ?>

          <span class="name"><?= the_field('peso'); ?></span>

        </a>

      <?php endwhile; ?>



    </div>

    <!-- <a href="#" id="loadMoreProducts" data-marca="<?= $id_tax->term_id; ?>" data-post-id="" class="btn-cta  col-5 col-lg-2">
      <span>Ver mais</span>
      <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/teste.gif" alt="">
    </a> -->

    <div class="barradenavegacao <?= $tax_color; ?>">
      <?php
      if ($iphone || $android || $ipad || $ipod || $berry == false) {
        $total_paginas = $products->max_num_pages;
        if ($paged == 1) {
          $total_paginas = $products->max_num_pages / 2;
        }
      }

      echo paginate_links(array(
        'format' =>
        '?pagina=%#%#anchor', 'show_all' => false, 'current' => max(1, $paged), 'total' => $total_paginas, 'prev_text' => '<i class="fas fa-caret-left fa-2x"></i>', 'next_text' => '<i class="fas fa-caret-right fa-2x"></i>',
        'type' => 'list'
      ));
      ?>
    </div>

  </div>
</section>

<div class='<?= $d_none; ?> container'>
  <?php
   get_template_part('onde-estamos'); 
      
      
  ?>
</div>

<div class='container px-3 px-lg-5 custom-section-map pt-5<?= $tax_color, $is_pages; ?>'>

  <h1 class='title text-center col-lg-8 px-0'>Quer saber onde encontrar o

    <?php if (is_tax()) : single_term_title();
    else : echo $product_name;
    endif;
    ?>?

    </br>Entre em contato conosco</h1>

  <a href="https://api.whatsapp.com/send?phone=55<?= $numerowhats ?>" target="_blank" class="btn-cta col-lg-2 col-8  btn-custom">Encontre</a>

</div>

<?= get_template_part('componente-revenda'); ?>

<?= get_template_part('nossas-marcas'); ?>



<?= get_footer(); ?>