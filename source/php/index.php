<?php get_header(); ?>

<div class="banner-home">

  <div class="container">

    <div class="position-custom">
      <?= get_field('banner_home') ?>
    </div>

    <div class="box-green col-lg-3">

      <span class="title">Qualidade certificada</span>

      <div class="white-box ">

        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/selo-abic.png" alt="">

        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/selo-abic-2.png" alt="">

      </div>

    </div>


  </div>

  <a class="click-banner d-none d-lg-block" href="#intro">
    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/banner-click.png" alt="">
  </a>

</div>

<section id="intro" class="intro-section is-home  ">

  <div class="container">
    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/icon-cup-home.png" alt="">

    <span class="title col-10 col-lg-6 px-0"><?= get_field('titulo_intro') ?></span>

    <div class="line "></div>

    <p><?= get_field('sub_titulo_intro') ?></p>

    <a href="<?= get_field('link_cta_intro') ?>" class="btn-cta col-9 col-md-5 col-lg-3"><?= get_field('texto_cta_intro') ?></a>

    <img class="cafe-left d-lg-none" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/cafe-home-1.png" alt="">

    <img class="cafe-left d-none d-lg-block" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/cafe-home-lg1.png" alt="">

    <img class="cafe-right d-lg-none" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/cafe-home-2.png" alt="">

    <img class="cafe-right d-none d-lg-block" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/cafe-home-lg2.png" alt="">

  </div>
</section>

<?= get_template_part('nossas-marcas'); ?>

<?= get_template_part('carousel-maquinas'); ?>


<section class="terc-export-home">

  <div class="container d-lg-flex">

    <div class="terc pl-lg-5 col-lg-6 px-0">

      <span class="head">terceirização</span>

      <img class="d-lg-none" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/terc-home-mob.png" alt="">

      <span class="title col-10 col-lg-11 px-0"><?= get_field('titulo_terc') ?></span>

      <p class="col-10 col-lg-8 px-0"><?= get_field('sub_titulo_terc') ?></p>

      <a href="<?= get_field('link_cta_terc') ?>" class="btn-cta col-6  col-md-3 col-lg-6"><?= get_field('texto_cta_terc') ?></a>

      <div class="d-none d-lg-block box-imgs-lg">

        <div class="wrapper-1">
          <div></div>
        </div>
        <div class="wrapper-2">
          <div></div>
        </div>
        <div class="wrapper-3">
          <div></div>
        </div>

      </div>


    </div>

    <div class="export col-lg-6 px-0">

      <div class="d-none d-lg-block box-imgs-lg">

        <div class="wrapper-1">
          <div></div>
        </div>
        <div class="wrapper-2">
          <div></div>
        </div>
        <div class="wrapper-3">
          <div></div>
        </div>

      </div>

      <span class="head">EXPORTAÇÃO / EXPORT / EXPORTACIÓN</span>

      <img class="d-lg-none" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/export-home-mob.png" alt="">

      <span class="title  col-lg-10 "><?= get_field('titulo_portugues_export') ?></span>

      <p class="col-lg-9 mb-1"><?= get_field('titulo_ingles_export') ?></p>
      <p class="col-lg-9"><?= get_field('titulo_espanhol_export') ?></p>

      <div class="d-lg-flex align-items-center col-lg-10">
        <a href="<?= get_field('link_cta_export') ?>" class="btn-cta col-md-3 col-7 col-lg-7 "><?= get_field('texto_cta_portugues_export') ?></a>
        <span class="sub-btn"><?= get_field('texto_cta_ingles_export') ?> / <?= get_field('texto_cta_espanhol_export') ?></span>
      </div>

    </div>

  </div>


</section>

<section class="carousel-receitas-home blog-recipes">

  <div class="header">

    <div class="d-flex align-items-center justify-content-center">
      <img class="mr-3" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/cafe-icon.png" alt="">
      <span class="title"><?= get_field('titulo_receitas') ?></span>
    </div>


    <div class="line col-5 col-lg-4 px-0"></div>

    <p class="sub"><?= get_field('sub_titulo_receitas') ?></p>

  </div>

  <?= get_template_part('carousel-receitas'); wp_reset_postdata(); ?>
</section>

<section class="carousel-blog-home blog-recipes">

  <div class="header">

    <div class="d-flex align-items-center justify-content-center">
      <img class="mr-3" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/blog-icon.png" alt="">
      <span class="title"><?= get_field('titulo_blog') ?></span>
    </div>


    <div class="line col-5 col-lg-1 px-0"></div>

    <p class="sub mb-lg-1"><?= get_field('sub_titulo_blog') ?></p>

  </div>

  <?= get_template_part('carousel-blog'); wp_reset_postdata(); ?>
</section>


<img style="width: 100%;" src="<?= get_field('banner_final'); ?>" alt="" class="d-lg-block d-none my-5">



<?php get_footer(); ?>