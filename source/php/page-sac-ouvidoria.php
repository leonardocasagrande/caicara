<?= get_header(); ?>

<section class="banner-sac">
  <div class="banner">

    <h1><?= the_title(); ?></h1>

  </div>
</section>

<section class="intro-section intro-section-contato container">

  <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/icon-sac.png" alt="">

  <span class="title col-7 px-0 col-lg-12">Podemos ajudar?</span>

  <div class="line "></div>

  <p class='pt-4 pt-lg-0'>Será um prazer te atender! </br>Preencha o formulário abaixo que entraremos em contato:</p>

</section>

<section class="section-form section-sac">

  <div class="container pb-lg-5">

    <div class="form-contato">

      <?php echo do_shortcode('[contact-form-7 id="61" title="formulario-sac-ouvidoria"]'); ?>


      <!-- <input type="text" name="" placeholder="Nome Completo" id="">

      <div class="d-lg-flex justify-content-between">
        <input class="custom-email" type="email" name="E-mail" placeholder="E-mail" id="">

        <input class="custom-tel" type="text" name="" placeholder="Telefone" id="">
      </div>

      <div class="d-lg-flex justify-content-between">
        <select class="custom-estado" name="estados-brasil">
          <option value="" disabled selected>Estado</option>
          <option value="AC">Acre</option>
          <option value="AL">Alagoas</option>
          <option value="AP">Amapá</option>
          <option value="AM">Amazonas</option>
          <option value="BA">Bahia</option>
          <option value="CE">Ceará</option>
          <option value="DF">Distrito Federal</option>
          <option value="ES">Espírito Santo</option>
          <option value="GO">Goiás</option>
          <option value="MA">Maranhão</option>
          <option value="MT">Mato Grosso</option>
          <option value="MS">Mato Grosso do Sul</option>
          <option value="MG">Minas Gerais</option>
          <option value="PA">Pará</option>
          <option value="PB">Paraíba</option>
          <option value="PR">Paraná</option>
          <option value="PE">Pernambuco</option>
          <option value="PI">Piauí</option>
          <option value="RJ">Rio de Janeiro</option>
          <option value="RN">Rio Grande do Norte</option>
          <option value="RS">Rio Grande do Sul</option>
          <option value="RO">Rondônia</option>
          <option value="RR">Roraima</option>
          <option value="SC">Santa Catarina</option>
          <option value="SP">São Paulo</option>
          <option value="SE">Sergipe</option>
          <option value="TO">Tocantins</option>
        </select>

        <input class="custom-cidade" type="text" name="" placeholder="Cidade" id="">

        <input class="custom-departamento" type="text" name="" placeholder="CPF" id="">
      </div>

      <div class="d-lg-flex justify-content-between">
        <input class="custom-lote" type="text" name="" placeholder="Lote do produto" id="">

        <input class="custom-produto" type="text" name="" placeholder="Produto" id="">
      </div>


      <select name="" id="">
        <option value="" selected disabled>Assunto</option>
      </select>

      <textarea name="" placeholder="Mensagem" id="" cols="30" rows="10"></textarea>


      <input type="submit" class="btn-cta col-4 col-lg-2 px-0" value="enviar"> -->

    </div>

    <div class="intro-section intro-section-sac d-lg-none pt-0">

      <span class="title">Fale conosco</span>

      <div class="line "></div>

      <div class="box-tel">

        <a class="tel" href="https://api.whatsapp.com/send?phone=5511930565574&text=Ol%C3%A1%2C%20vim%20do%20site%20e%20gostaria%20de%20tirar%20uma%20d%C3%BAvida.%20Podem%20me%20ajudar%3F" target="_blank" class="text"><i class="fab fa-whatsapp"></i>(11) 93056-5574</a>

        <a href="mailto:rh@caicara.com.br" class="text"><i class="fas fa-envelope-open"></i>atendimento@caicara.com.br</a>


      </div>

    </div>


  </div>

</section>

<?= get_template_part('onde-estamos'); ?>

<?= get_template_part('infos-red'); ?>



<?= get_footer(); ?>