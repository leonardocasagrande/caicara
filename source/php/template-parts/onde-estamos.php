<?php
the_post();

$product_name = $args['produto']->name;

if (is_tax() || is_single()) : $d_none = ' d-none';
  $d_block = ' d-block';
else : $d_block = ' d-none';
endif;

if (is_page('contato', 'sac-ouvidoria')) : $d_header_block = ' d-block d-lg-flex';
  $d_map_contato_off = ' d-none';
else : $d_header_block = ' d-none';
endif;

if (is_tax('nossas_marcas', array('caicara', 'sao_joaquim'))) : $d_map_on = ' d-none';
  $d_map_off = ' d-block';
else : $d_map_off = ' d-block';
  $d_map_on = ' d-none';
endif;

if (is_page('sac-ouvidoria')) : $d_map_on = ' d-none';
  $d_map_off = ' d-none';
endif;



if (is_tax('nossas_marcas', 'caicara')) : $tax_color = ' tax-red';
elseif (is_tax('nossas_marcas', 'sao_joaquim')) : $tax_color = ' tax-yellow';
elseif (is_tax('nossas_marcas', 'san_pietro')) : $tax_color = ' tax-brown';
elseif (is_tax('nossas_marcas', 'itatibense')) : $tax_color = ' tax-dbrown';
elseif (is_tax('nossas_marcas', 'bourbon')) : $tax_color = ' tax-black';
elseif (is_tax('nossas_marcas', 'negrao')) : $tax_color = ' tax-wine';
elseif (is_tax('nossas_marcas', 'campo_verde')) : $tax_color = ' tax-green';
endif;

?>

<section class="contato-without-map <?= $d_map_on; ?>">

  <div class="intro-section <?= $tax_color; ?>">

    <span class="title">Quer saber onde encontrar o

      <?php if (is_tax()) : single_term_title();
      else : echo $product_name;
      endif; ?>?
    </span>

    <span class=" title">Entre em contato conosco</span>


    <a href="https://api.whatsapp.com/send?phone=55<?= $numerowhats ?>" target="_blank" class="btn-cta col-11 col-lg-2 mt-4">Encontrar</a>

  </div>


</section>




<section class="onde-estamos <?= $d_map_off; ?>">

  <div class="intro-section container <?= $tax_color; ?>">


    <span class="title <?= $d_none; ?>">Onde encontrar nossos produtos?</span>

    <span class="title <?= $d_block; ?>">Onde encontrar o


      <?php if (is_tax()) : single_term_title();
      else : echo $product_name;
      endif; ?>?

    </span>

    <div class="line col-lg-2 px-0"></div>

    <div class="col-lg-8  align-items-baseline justify-content-between m-auto px-0 <?= $d_header_block; ?>">

      <span class="title-blue col-lg-5 px-0">Matriz, fábrica e loja de fábrica</span>

      <div class="d-flex align-items-baseline col-lg-5 col-11 px-0">
        <i class="fas fa-map-marker-alt pr-3"></i>
        <p>Rua Cica, 452 - Vila Angélica, Jundiaí/SP - CEP: 13206-765</p>
      </div>

    </div>

    <!-- <div class="input-search <?= $d_map_contato_off; ?>">
      <textarea name="" id="" cols="30" placeholder="Digite um endereço, CEP, cidade ou estado" rows="2"></textarea>
      <a class="alvo" href="#"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/alvo.png" alt=""></a>
      <a class="lupa" href="#"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/search.png" alt=""></a>
    </div> -->

  </div>

  <!-- <div class="<?= $d_map_contato_off; ?>">
    <img class="d-lg-none" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/map-sm.png" alt="">

    <div class="container d-none d-lg-block">
      <img class="" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/map-lg.png" alt="">
    </div>


    <div class="response-search p container">

      <div class="item">

        <span><b>Supermercado Forte</b></span>
        <p class='col-10 px-0 col-lg-12'>Av. Antônio Piranga, 50 Centro, Diadema/SP</p>
        <a href="#">Localização</a>

      </div>

      <div class="item">

        <span><b>Supermercado Forte</b></span>
        <p class='col-10 px-0 col-lg-12'>Av. Antônio Piranga, 50 Centro, Diadema/SP</p>
        <a href="#">Localização</a>

      </div>

      <div class="item">

        <span><b>Supermercado Forte</b></span>
        <p class='col-10 px-0 col-lg-12'>Av. Antônio Piranga, 50 Centro, Diadema/SP</p>
        <a href="#">Localização</a>

      </div>

      <div class="item">

        <span><b>Supermercado Forte</b></span>
        <p class='col-10 px-0 col-lg-12'>Av. Antônio Piranga, 50 Centro, Diadema/SP</p>
        <a href="#">Localização</a>

      </div>


    </div>

    <a href="#" class="btn-ver-mais col-5 col-lg-1 px-0">Ver mais</a> 
  </div> -->
      <?php 
      $id_tax = get_queried_object();
      echo do_shortcode( '[wpsl category="'.$id_tax->slug.'"]' ) ?>

</section>