<?php

$product_name = $args['produto']->name;

if (is_page('nossas-marcas')) : $tax_show = 'd-none';
endif;


if (is_tax() || is_single()) : $d_none = ' d-none';
  $d_block = ' d-block';
else : $d_block = ' d-none';
endif;

?>

<section class="componente-revenda container">

  <div class="revenda col-lg-7 ">

    <span class="mini">revenda</span>

    <span class="title">Você tem um estabelecimento?</span>

    <p class="<?= $d_none; ?> col-lg-10 px-0">Você pode levar o Café Caiçara às suas prateleiras e incluir mais sabor nas compras de seus clientes.</p>

    <p class="<?= $tax_show; ?> col-lg-10 px-0">Você pode levar o

      <?php if (is_tax()) : single_term_title();
      else : echo $product_name;
      endif;
      ?>

      às suas prateleiras e incluir mais sabor nas compras de seus clientes.</p>

    <a href="mailto:atendimento@caicara.com.br" class="btn-cta col-9 col-md-5 col-lg-7">Seja um revendedor</a>

  </div>

  <div class="seja-cliente col-lg-4">

    <span class="mini">seja cliente</span>

    <span class="title col-7 px-0 col-lg-12">Tem uma empresa?</span>

    <p class="<?= $d_none; ?>">Você também pode servir o Café Caiçara aos seus clientes e colaboradores com mais facilidade e economia.</p>

    <p class="<?= $tax_show; ?>">Você também pode servir o

      <?php if (is_tax()) : single_term_title();
      else : echo $product_name;
      endif;
      ?>

      aos seus clientes e colaboradores com mais facilidade e economia.</p>

    <a href="mailto:atendimento@caicara.com.br" class="btn-cta col-md-5 col-lg-12 px-0">Torne-se um cliente institucional</a>

  </div>

</section>