<section class="red-box-info mt-lg-5">

  <div class="container d-lg-flex col-lg-10 justify-content-between align-items-center">

    <div>
      <?php $numeroFixo = get_field('atendimento_fixo', 41);
            $numerowhats = get_field('atendimento_whats', 41);
            $email = get_field('atendimento_e_mail', 41);
      ?>
      <a href="tel:+55<?= $numeroFixo ?>" class="text"><i class="fas fa-phone-alt"></i><?= formatarNumeroFixo($numeroFixo) ?></a>

      <a href="https://api.whatsapp.com/send?phone=55<?= $numerowhats ?>&text=Ol%C3%A1%2C%20vim%20do%20site%20e%20gostaria%20de%20tirar%20uma%20d%C3%BAvida.%20Podem%20me%20ajudar%3F" target="_blank" class="text"><i class="fab fa-whatsapp"></i><?= formatarNumeroCelular($numerowhats) ?></a>

      <a href="mailto:<?= $email ?>" class="text"><i class="fas fa-envelope-open"></i><?= $email ?></a>
    </div>


    <div class="d-flex align-items-baseline col-lg-5 px-0 pt-4 pt-lg-0">

      <i class="far fa-clock"></i>

      <div class="">
        <p><b>ATENDIMENTO</b></p>
        <p>De segunda a sexta-feira, </br>das 07h30 às 17h00 (Exceto finais de semana e feriados)</p>
      </div>

    </div>

  </div>
</section>