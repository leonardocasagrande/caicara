<?= get_header(); ?>

<section class="banner-exportacao">
  <div class="banner">

    <h1><?= the_title(); ?></h1>
    <span class="sub"><?= get_field('sub_titulo_banner') ?></span>

  </div>
</section>


<section class="intro-section is-export container">

  <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/icon-terc.png" alt="">

  <span class="title col-lg-6 px-0"><?= get_field('titulo_company') ?></span>

  <div class="line "></div>

</section>

<section class="about-company">
  <div class="container d-lg-flex justify-content-between">
    <div class="text col-lg-6 px-0">
    <?= get_field('sobre_company') ?>
    </div>

    <div class="img-wrapper col-lg-5 px-0">
      <div></div>
    </div>

  </div>

</section>


<div class="nossas-marcas-english">

<?php
    $marcas = get_terms( array(
        'taxonomy'=> 'nossas_marcas',
        'orderby' => 'meta_value',
        'meta_key' => 'posicao'
    ));
    // var_dump($marcas);
    foreach ($marcas as $marca):
    
?>
<section class="nossos-produtos tax-black">

  <div class="container">

    <span class="title"><?= $marca->name ?></span>

    <div class="line col-7 col-lg-2 px-0"></div>

    <div class="product-box justify-content-start col-lg-12">

      <?php
      wp_reset_postdata();
      $argsProdutos =   array(
        'post_type' => 'produtos',
        'order' => 'ASC',
        'orderby' => 'menu_order',
        'posts_per_page' => -1,
        'tax_query' => array(
          array(
            'taxonomy' => 'nossas_marcas',
            'field' => 'term_id',
            'terms' => $marca->term_id,
          )

        )
      );

      $products = new WP_Query($argsProdutos);


      while ($products->have_posts()) : $products->the_post();


      ?>

        <a href="<?= the_permalink(); ?>" class="item ">

          <img src="<?= get_the_post_thumbnail_url(); ?>" alt="">

          <div class="line-red d-lg-none col-5 px-0"></div>

          <span class="name"><?= the_title(); ?></span>

          <?php if (get_field('embalagem')) :; ?>
            <span class="name">Packing <?= the_field('embalagem'); ?></span>
          <?php endif; ?>

          <span class="name"><?= the_field('peso'); ?></span>

        </a>

      <?php endwhile; wp_reset_postdata();?>



    </div>


  </div>
</section>
<?php endforeach; ?>

</div>

<section class="exportation-form">


  <div class="intro-section is-export container">

    <span class="title col-lg-6 px-0">Fill out our contact form:</span>

    <div class="line "></div>

  </div>

  <div class="form-box container">

    <?= do_shortcode('[contact-form-7 id="321" title="Formulário Exportação"]'); ?>

  </div>

  <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/grao-lg-2.png" alt="">


</section>

<section class="para-informacoes container mb-5">

  <div class="intro-section is-terc pt-0">

    <span class="title col-lg-7 pt-0 px-4">For more information</span>

    <div class="line col-lg-2"></div>

  </div>

  <div class="box-infos  ">

    <a class=" whats" target="_blank" href="https://api.whatsapp.com/send?phone=55<?= get_field('whatsapp') ?>">

      <i class="fab fa-whatsapp"></i>
      <span>+55 <?php echo formatarNumeroCelular(get_field('whatsapp')); ?></span>

    </a>

    <div class="d-lg-flex col-lg-8 px-0 align-items-baseline justify-content-between">

      <div>
        <a class="btn-contato mb-3" target="_blank" href="tel:+55<?= get_field('fixo') ?>">

          <i class="fas fa-phone-alt"></i>
          <span>+55 <?php echo formatarNumeroFixo(get_field('fixo')); ?></span>

        </a>

        <a target="_blank" href="mailto:export@caicara.com.br" class="btn-contato mb-3">

          <i class="fas fa-envelope-open"></i>
          <span>export@caicara.com.br</span>

        </a>
      </div>

      <div>
        <div class="btn-contato mb-3">

          <i class="fas fa-user"></i>
          <span><?= get_field('nome') ?></span>

        </div>

        <div class="btn-contato ">

          <i class="fab fa-skype"></i>
          <span><?= get_field('nome') ?></span>

        </div>
      </div>



    </div>

  </div>

</section>

<?= get_footer(); ?>