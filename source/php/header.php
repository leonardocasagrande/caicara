<!DOCTYPE html>

<html lang="pt_BR">

<head>
  <!-- GoAdopt -->
  <meta name="adopt-website-id" content="5dcf2905-2884-4cd2-93a1-74ee760de0c6" />
  <script src="//tag.goadopt.io/injector.js?website_code=5dcf2905-2884-4cd2-93a1-74ee760de0c6" class="adopt-injector"></script>


  <!-- Google Tag Manager -->
  <script>
    (function(w, d, s, l, i) {
      w[l] = w[l] || [];
      w[l].push({
        'gtm.start': new Date().getTime(),
        event: 'gtm.js'
      });
      var f = d.getElementsByTagName(s)[0],
        j = d.createElement(s),
        dl = l != 'dataLayer' ? '&l=' + l : '';
      j.async = true;
      j.src =
        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
      f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-PLML8RG');
  </script>
  <!-- End Google Tag Manager -->





  <meta charset="UTF-8">

  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <title> <?= wp_title(); ?></title>

  <meta name="robots" content="index, follow" />

  <meta name="msapplication-TileColor" content="#ffffff">

  <meta name="theme-color" content="#ffffff">

  <?php wp_head(); ?>



  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.2/tiny-slider.css">

  <link rel="stylesheet" type="text/css" href="<?= get_stylesheet_directory_uri(); ?>/dist/css/style.css">





</head>

<body>

  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PLML8RG" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->

<?php $numerowhats = get_field('atendimento_whats', 41); ?>



  <header class="d-lg-block d-none  ">

    <div class="header-bg"></div>

    <div class="header ">
      <div class="container">

        <a href="<?= get_site_url(); ?>/">
          <img class="logo-header" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo-header.png" alt="">
        </a>

        <div class="links  px-0">

          <a href="<?= get_site_url(); ?>/"><?= get_the_title(259) ?></a>
          <a href="<?= get_site_url(); ?>/sobre-o-grupo"><?= get_the_title(5) ?></a>
          <div class="dropdown">
            <span class="">Máquinas de café</span>

            <div class="dropdown-content">

              <a href="<?= get_site_url(); ?>/locacao-e-vendas-de-maquinas-de-cafe"><?= get_the_title(91) ?></a>
              <a href="<?= get_site_url(); ?>/insumos-para-maquinas-de-cafe"><?= get_the_title(195) ?></a>
              <a href="<?= get_site_url(); ?>/manutencao-e-oficina"><?= get_the_title(94) ?></a>

            </div>

          </div>
          <a href="<?= get_site_url(); ?>/terceirizacao"><?= get_the_title(1196) ?></a>
          <a href="<?= get_site_url(); ?>/exportacao"><?= get_the_title(1197) ?></a>
          <div class="dropdown">
            <a href="<?= get_site_url(); ?>/produtos"><?= get_the_title(2116) ?></a>
            <div class="dropdown-content">

              <a href="<?= get_site_url(); ?>/nossas-marcas"><?= get_the_title(71) ?></a>

            </div>
          </div>
          <a href="<?= get_site_url(); ?>/sac-ouvidoria"><?= get_the_title(43) ?></a>
          <a href="<?= get_site_url(); ?>/contato"><?= get_the_title(41) ?></a>
          <a href="<?= get_site_url(); ?>/receitas"><?= get_the_title(7) ?></a>
          <a href="<?= get_site_url(); ?>/blog"><?= get_the_title(28) ?></a>
          <a class="lock-menu" href="#"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/cadeado.png" alt=""></a>

        </div>


        <div class="btn-midias">

          <a href="https://www.lojacafecaicara.com.br/" target="_blank" class="buy-online">compre online</a>
          <a href="https://www.instagram.com/cafecaicaraoficial/" target="_blank"><i class="fab fa-instagram"></i></a>
          <a href="https://www.facebook.com/CaicaraCafeJundiai" target="_blank"><i class="fab fa-facebook-f"></i></a>

        </div>

      </div>
    </div>


  </header>



  <div class="bg-transparent d-lg-none"></div>
  <nav class=" d-lg-none top-nav  " id="top-nav">

    <a href="<?= get_site_url(); ?>"><img class="logo-header" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo-header.png" alt="goma"></a>

    <input class="menu-btn" type="checkbox" id="menu-btn" />

    <label class="menu-icon" for="menu-btn"><span class="navicon"></span></label>

    <div class="menu">

      <a href="<?= get_site_url(); ?>/"><?= get_the_title(259) ?></a>
      <a href="<?= get_site_url(); ?>/sobre-o-grupo"><?= get_the_title(5) ?> </a>


      <span>Máquinas de café</span>
      <a class="a-mini mt-2" href="<?= get_site_url(); ?>/locacao-e-vendas-de-maquinas-de-cafe"><?= get_the_title(91) ?></a>
      <a class="a-mini" href="<?= get_site_url(); ?>/insumos-para-maquinas-de-cafe"><?= get_the_title(195) ?></a>
      <a class="a-mini mb-3" href="<?= get_site_url(); ?>/manutencao-e-oficina"><?= get_the_title(94) ?></a>

      <a href="<?= get_site_url(); ?>/terceirizacao"><?= get_the_title(1196) ?></a>
      <a href="<?= get_site_url(); ?>/exportacao"><?= get_the_title(1197) ?></a>
      <a href="<?= get_site_url(); ?>/nossas-marcas"><?= get_the_title(71) ?></a>
      <a href="<?= get_site_url(); ?>/produtos"><?= get_the_title(2117) ?></a>
      <a href="<?= get_site_url(); ?>/sac-ouvidoria"><?= get_the_title(43) ?></a>
      <a href="<?= get_site_url(); ?>/receitas"><?= get_the_title(7) ?></a>
      <a href="<?= get_site_url(); ?>/blog"><?= get_the_title(28) ?></a>
      <a href="<?= get_site_url(); ?>/contato"><?= get_the_title(41) ?></a>


      <div class="midias-header px-0 col-7 col-md-5 ">
        <a href="https://www.lojacafecaicara.com.br/" target="_blank" class="buy-online">compre online</a>
        <a href="https://www.instagram.com/cafecaicaraoficial/" target="_blank"><i class="fab fa-instagram"></i></a>
        <a href="https://www.facebook.com/CaicaraCafeJundiai" target="_blank"><i class="fab fa-facebook-f"></i></a>
      </div>

    </div>



  </nav>

  <a href="https://api.whatsapp.com/send?phone=55<?= $numerowhats ?>&text=Ol%C3%A1%2C%20vim%20atrav%C3%A9s%20do%20site%20e%20gostaria%20de%20saber%20mais%20sobre%20os%20seus%20produtos%2C%20poderia%20me%20ajudar%3F" target="_blank" class="whats-btn ">

    <i class="fab fa-whatsapp"></i>

    <div class="text-whats col-lg-9 px-0">

      <span class="mini">WhatsApp Televendas:</span>
      <span class="number"><?= formatarNumeroCelular($numerowhats) ?></span>

    </div>

  </a>