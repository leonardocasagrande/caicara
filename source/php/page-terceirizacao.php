<?= get_header(); ?>

<section class="banner-terceirizacao">
  <div class="banner">

    <h1><?= the_title(); ?></h1>
    <span class="sub"><?= get_field('sub_titulo_banner') ?></span>

  </div>
</section>


<section class="intro-section is-terc container">

  <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/icon-terc.png" alt="">

  <span class="title col-lg-6 px-0"><?= get_field('titulo_intro') ?></span>

  <div class="line "></div>

</section>

<section class="main-terc">

  <div class="container">

    <div class="green-box">

      <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/package-terc.png" alt="">

      <div>
        <?= get_field('green_box') ?>
      </div>

    </div>

    <div class="o-que-fazer">


      <div class="d-none d-lg-block xicara-wrapper">
        <div></div>
      </div>

      <div class="col-lg-6 px-0 ">

        <span class="mini-title-terc"><?= get_field('titulo_oqfazer') ?></span>

        <?= get_field('instrucoes') ?>

        <div class="img-box">

          <div class="xicara-grao">
            <div></div>
          </div>

          <div class="caminhao">
            <div></div>
          </div>

        </div>

      </div>


      <div class="col-lg-5 px-0 pr-lg-5 mr-lg-4">

        <span class="mini-title-terc"><?= get_field('titulo_fazemos') ?></span>

        <?= get_field('esclarecimentos') ?>

      </div>

    </div>

  </div>

</section>


<section class="terc-form py-lg-0">

  <div class="container">

    <div class="intro-section is-terc pt-lg-5">

      <span class="title col-lg-7 px-lg-4 px-0"><?= get_field('titulo_form') ?></span>

      <div class="line "></div>

    </div>

    <div class="form-box container">
      <?= do_shortcode('[contact-form-7 id="266" title="Formulário Terceirização"]'); ?>
    </div>

  </div>

</section>


<section class="terc-end pt-lg-0">

  <div class="container">

    <span class="mini-title-terc d-lg-none blue col-11 px-0"><?= get_field('rodape_form') ?></span>

    <span class="mini-title-terc d-none d-lg-block blue col-11 col-lg-6 px-0 pr-lg-5"><?= get_field('rodape_form') ?></span>



    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/grao-lg-2.png" alt="">

  </div>

</section>

<section class="para-informacoes container">

  <div class="intro-section is-terc pt-lg-5 pb-lg-2">

    <span class="title col-lg-7 px-4">Para mais informações</span>

    <div class="line col-lg-2"></div>

  </div>

  <div class="box-infos ">

    <a target="_blank" href="mailto:<?= get_field('email_principal') ?>" class="whats">

      <i class="fas fa-envelope-open"></i>
      <span><?= get_field('email_principal') ?></span>

    </a>

    <div class="d-lg-flex col-lg-9 px-0 align-items-baseline justify-content-between">
      <a class="btn-contato" target="_blank" href="tel:+55<?= get_field('fixo') ?>">

        <i class="fas fa-phone-alt"></i>
        <span><?php echo formatarNumeroFixo(get_field('fixo')); ?></span>

      </a>

      <div class="btn-contato mb-3" target="_blank" href="mailto:<?= get_field('email_responsavel') ?>">

        <i class="fas fa-user"></i>
        <span><?= get_field('nome') ?></span>

      </div>


    </div>

  </div>

</section>



<?= get_template_part('nossas-marcas'); ?>

<!-- <?= get_template_part('carousel-maquinas'); ?> -->

<div class="pb-5"></div>


<?= get_footer(); ?>