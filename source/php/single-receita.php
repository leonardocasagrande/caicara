<? the_post();?>

<?= get_header(); ?>

<section class="banner-receitas-single  ">
  <div class="banner">

    <h1>Receitas</h1>

  </div>
</section>

<section class="intro-section intro-single-receitas container">

  <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/icon-receitas.png" alt="">

  <span class="title"><?= the_title(); ?></span>

  <div class="line "></div>

</section>

<section class="receita">

  <div class="top-150">
    <div class="img-receita" style="background: url(<?= the_post_thumbnail_url(); ?>);"></div>

    <div class="container d-lg-flex justify-content-around align-items-baseline">

      <div>
        <span class="mini-title">Ingredientes</span>

        <p><?= the_field('ingredientes'); ?></p>
      </div>

      <div>
        <span class="mini-title pt-5">Modo de preparo</span>

        <p><?= the_field('modo_de_preparo'); ?></p>
      </div>


    </div>
  </div>
</section>


<section class="bg-gray">

  <div class="intro-section receitas-single container pb-0">

    <span class="title col-8 px-0">Mais receitas</span>

    <div class="line "></div>

  </div>

  <?= get_template_part('carousel-receitas'); ?>

</section>


<?= get_template_part('nossas-marcas'); ?>







<?= get_footer(); ?>