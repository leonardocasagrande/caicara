<?php

$new_limit = 6;

$iphone = strpos($_SERVER['HTTP_USER_AGENT'], "iPhone");
$android = strpos($_SERVER['HTTP_USER_AGENT'], "Android");
$ipad = strpos($_SERVER['HTTP_USER_AGENT'], "iPad");
$berry = strpos($_SERVER['HTTP_USER_AGENT'], "BlackBerry");
$ipod = strpos($_SERVER['HTTP_USER_AGENT'], "iPod");

if ($iphone || $android || $ipad || $ipod || $berry == true) {
  $new_limit = 4;
}

echo get_header(); ?>

<section class="banner-receitas">
  <div class="banner">

    <h1><?= the_title(); ?></h1>

  </div>
</section>

<section class="intro-section is-contato-ref container">

  <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/icon-receitas.png" alt="">

  <span class="title d-lg-none col-10 col-lg-12 px-0">Se já é bom, imagine se acrescentar café</span>
  <span class="title d-none d-lg-block">Se já é bom,</br> imagine se acrescentar café</span>

  <div class="line "></div>

</section>

<secion class="destaques-receitas container">

  <span class="sub container">destaques</span>

  <div class="wrapper container">

    <div class="carousel-destaques-receitas">

      <?php
      $argsReceitas = array(
        'post_type' => 'receita',
        'order' => 'ASC',
        'meta_query' => array(
          array(
            'key' => 'destaque',
            'value' => '1',
            'compare' => '=='
          )
        )
      );
      $receitas = new WP_Query($argsReceitas);

      if ($receitas->have_posts()) : while ($receitas->have_posts()) :  $receitas->the_post();

          $taxonomiaReceita = get_the_terms(get_the_id(), 'categoria_receita');

      ?>


          <div class="item">

            <div class="img-header" style="background: url(<?= the_post_thumbnail_url(); ?>)"></div>

            <div class="content">

              <span class="categoria">/
                <?php foreach ($taxonomiaReceita as $taxonomia) : echo $taxonomia->name;
                endforeach; ?>
              </span>


              <span class="title"><?= the_title(); ?></span>

              <a href="<?= the_permalink(); ?>" class="btn-cta col-lg-6 col-8 px-0">Confira a receita</a>

            </div>

          </div>

      <?php
        endwhile;
      endif; ?>

    </div>

  </div>

</secion>

<section class="receitas">


  <?php
  $counter = 0;

  $taxonomies = get_terms(array(
    'taxonomy' => 'categoria_receita',
    'hide_empty' => false,
  ));

  ?>


  <ul class="nav nav-tabs categorys container" id="myTab" role="tablist">

    <li class="nav-item" role="presentation">
      <button class="nav-link active" id="todas-tab" data-bs-toggle="tab" data-bs-target="#todas" type="button" role="tab" aria-controls="todas" aria-selected="true">Todas</button>
    </li>

    <span class="divisor">|</span>



    <?php foreach ($taxonomies as $taxonomy) : ?>



      <li class="nav-item" role="presentation">
        <button class="nav-link" id="nav-recipe-<?= $taxonomy->term_id ?>" data-bs-toggle="tab" data-bs-target="#tab-recipe-<?= $taxonomy->term_id ?>" type="button" role="tab" aria-controls="tab-recipe-<?= $taxonomy->term_id ?>" aria-selected="false"><?= $taxonomy->name ?></button>
      </li>



      <?php $counter++;

      if ($counter <= 4) :  ?>

        <span class="divisor">|</span>

    <?php endif;

    endforeach; ?>

  </ul>



  <div class="receitas-response tab-content" id="myTabContent">

    <div class="tab-pane fade show active container" data-tax="0" id="todas" role="tabpanel" aria-labelledby="todas-tab">


      <div class=" d-md-flex justify-content-between flex-wrap">

        <?php
        $argsReceitas = array(
          'post_type' => 'receita',
          'order' => 'ASC',
          'posts_per_page' => $new_limit,

        );

        $receitas = new WP_Query($argsReceitas);



        if ($receitas->have_posts()) : while ($receitas->have_posts()) :  $receitas->the_post();

            $taxonomiaReceita = get_the_terms(get_the_id(), 'categoria_receita');

        ?>



            <div class=" item col-md-6 col-lg-4">

              <div class="img-header" style="background: url(<?= the_post_thumbnail_url(); ?>)"></div>

              <div class="content">

                <span class="categoria">/
                  <?php foreach ($taxonomiaReceita as $taxonomia) : echo $taxonomia->name;
                  endforeach; ?>
                </span>
                <span class="title"><?= the_title(); ?></span>

                <a href="<?= the_permalink(); ?>" class="btn-cta col-lg-6 col-8 px-0">Confira a receita</a>

              </div>

            </div>

        <?php
          endwhile;
        endif;  ?>
      </div>


      <a href="#" id="showMoreRecipes-0" class="btn-cta red col-lg-1 col-4 px-0">
        <span>Ver mais</span>
        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/teste.gif" alt="">
      </a>


    </div>


    <?php foreach ($taxonomies as $taxonomy) : ?>


      <?php



      $argsReceitasSearch =   array(
        'post_type' => 'receita',
        'order' => 'ASC',
        'posts_per_page' => $new_limit,
        'tax_query' => array(
          array(
            'taxonomy' => 'categoria_receita',
            'field' => 'term_id',
            'terms' => $taxonomy->term_id,
          )

        )
      ); ?>


      <div class="tab-pane fade container  " data-tax="<?php echo $taxonomy->term_id; ?>" id="tab-recipe-<?= $taxonomy->term_id ?>" role="tabpanel" aria-labelledby="nav-recipe-<?= $taxonomy->term_id ?>">


        <div class="d-md-flex justify-content-between flex-wrap">

          <?php $catquery = new WP_Query($argsReceitasSearch); ?>


          <?php while ($catquery->have_posts()) : $catquery->the_post();
          ?>


            <div class=" item col-lg-4 col-md-6">

              <div class="img-header" style="background: url(<?= the_post_thumbnail_url(); ?>)"></div>

              <div class="content">

                <span class="categoria">/<?= $taxonomy->name; ?></span>
                <span class="title"><?= the_title(); ?></span>

                <a href="<?= the_permalink(); ?>" class="btn-cta col-lg-6 col-8 px-0">Confira a receita</a>

              </div>

            </div>


          <?php endwhile;
          wp_reset_query();
          ?>

        </div>


        <a href="#" id="showMoreRecipes-<?php echo $taxonomy->term_id; ?>" class=" btn-cta red col-lg-1 col-4 col-md-2 px-0">
          <span>Ver mais</span>
          <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/teste.gif" alt="">
        </a>


      </div>

    <?php endforeach;
    ?>




  </div>
</section>


<section class="blog-recipes recipe-custom pb-lg-0">


  <div class="header">

    <div class="d-flex align-items-center justify-content-center">
      <img class="mr-3" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/blog-recipe-icon.png" alt="">
      <span class="title">Blog</span>
    </div>


    <div class="line col-5 col-lg-1 px-0"></div>

    <p class="sub">Para ler tomando um cafézinho.</p>

  </div>

  <?= get_template_part('carousel-blog'); ?>
</section>


<?= get_footer(); ?>