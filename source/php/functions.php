<?php


function my_function_admin_bar()
{
	return false;
}
add_filter('show_admin_bar', 'my_function_admin_bar');

add_theme_support( 'post-thumbnails' );



function cptui_register_my_cpts() {

	/**
	 * Post Type: Receitas.
	 */

	$labels = [
		"name" => __( "Receitas", "custom-post-type-ui" ),
		"singular_name" => __( "Receita", "custom-post-type-ui" ),
	];

	$args = [
		"label" => __( "Receitas", "custom-post-type-ui" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => [ "slug" => "receita", "with_front" => true ],
		"query_var" => true,
		"supports" => [ "title", "editor", "thumbnail" ],
		"taxonomies" => [ "todas", "bebidas", "bebidas_light", "doces", "doces_light", "salgados" ],
	];

	register_post_type( "receita", $args );
}

add_action( 'init', 'cptui_register_my_cpts' );


function cptui_register_my_taxes() {

	/**
	 * Taxonomy: Categoria Receitas.
	 */

	$labels = [
		"name" => __( "Categoria Receitas", "custom-post-type-ui" ),
		"singular_name" => __( "Categoria Receita", "custom-post-type-ui" ),
	];

	$args = [
		"label" => __( "Categoria Receitas", "custom-post-type-ui" ),
		"labels" => $labels,
		"public" => true,
		"publicly_queryable" => true,
		"hierarchical" => true,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => [ 'slug' => 'categoria_receita', 'with_front' => true, ],
		"show_admin_column" => false,
		"show_in_rest" => true,
		"rest_base" => "categoria_receita",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"show_in_quick_edit" => false,
			];
	register_taxonomy( "categoria_receita", [ "receita" ], $args );
}
add_action( 'init', 'cptui_register_my_taxes' );



add_action('rest_api_init', 'add_thumbnail_to_JSON');
function add_thumbnail_to_JSON()
{
  //Add featured image
  register_rest_field(
    array(
		'produtos',
		'post','receita',
		), // Where to add the field (Here, blog posts. Could be an array)
    'featured_image_src', // Name of new field (You can call this anything)
    array(
      'get_callback'    => 'get_image_src',
      'update_callback' => null,
      'schema'          => null,
    )
  );
}

function get_image_src($object, $field_name, $request)
{
  $feat_img_array = wp_get_attachment_image_src(
    $object['featured_media'], // Image attachment ID
    'full',  // Size.  Ex. "thumbnail", "large", "full", etc..
    true // Whether the image should be treated as an icon.
  );
  return $feat_img_array[0];
}






add_action('rest_api_init', 'add_tax_to_JSON');
function add_tax_to_JSON()
{
  //Add featured image
  register_rest_field(
		'receita',
		 // Where to add the field (Here, blog posts. Could be an array)
    'categoria_receita_slug', // Name of new field (You can call this anything)
    array(
      'get_callback'    => 'my_awesome_func',
      'update_callback' => null,
      'schema'          => null,
    )
  );
}

/**
 * Grab latest post title by an author!
 *
 * @param array $data Options for the function.
 * @return string|null Post title for the latest, * or null if none.
 */
function my_awesome_func( $data ) {
  $tax = get_terms(array(
    'taxonomy' => 'categoria_receita',
    'hide_empty' => false,
  ));


 
  if ( empty( $tax ) ) {
    return null;
  }
 
  return $tax;
}

/**
 * Display a custom taxonomy dropdown in admin
 * @author Mike Hemberger
 * @link http://thestizmedia.com/custom-post-type-filter-admin-custom-taxonomy/
 */
add_action('restrict_manage_posts', 'tsm_filter_post_type_by_taxonomy');
function tsm_filter_post_type_by_taxonomy() {
	global $typenow;
	$post_type = 'produtos'; // change to your post type
	$taxonomy  = 'nossas_marcas'; // change to your taxonomy
	if ($typenow == $post_type) {
		$selected      = isset($_GET[$taxonomy]) ? $_GET[$taxonomy] : '';
		$info_taxonomy = get_taxonomy($taxonomy);
		wp_dropdown_categories(array(
			'show_option_all' => sprintf( __( 'Show all %s', 'textdomain' ), $info_taxonomy->label ),
			'taxonomy'        => $taxonomy,
			'name'            => $taxonomy,
			'orderby'         => 'name',
			'selected'        => $selected,
			'show_count'      => true,
			'hide_empty'      => true,
		));
	};
}
/**
 * Filter posts by taxonomy in admin
 * @author  Mike Hemberger
 * @link http://thestizmedia.com/custom-post-type-filter-admin-custom-taxonomy/
 */
add_filter('parse_query', 'tsm_convert_id_to_term_in_query');
function tsm_convert_id_to_term_in_query($query) {
	global $pagenow;
	$post_type = 'produtos'; // change to your post type
	$taxonomy  = 'nossas_marcas'; // change to your taxonomy
	$q_vars    = &$query->query_vars;
	if ( $pagenow == 'edit.php' && isset($q_vars['post_type']) && $q_vars['post_type'] == $post_type && isset($q_vars[$taxonomy]) && is_numeric($q_vars[$taxonomy]) && $q_vars[$taxonomy] != 0 ) {
		$term = get_term_by('id', $q_vars[$taxonomy], $taxonomy);
		$q_vars[$taxonomy] = $term->slug;
	}
}

function formatarNumeroFixo($number){
	if(  preg_match( '/^(\d{2})(\d{4})(\d{4})$/', $number,  $matches ) )
{
    $result = '(' . $matches[1] . ') ' .$matches[2] . '-' . $matches[3];
}else{
	$result = "Nao deu";
}
return $result;
}
function formatarNumeroCelular($number){
	if(  preg_match( '/^(\d{2})(\d{5})(\d{4})$/', $number,  $matches ) )
{
    $result = '(' . $matches[1] . ') ' .$matches[2] . '-' . $matches[3];
}else{
	$result = "Nao deu";
}
return $result;
}
?>