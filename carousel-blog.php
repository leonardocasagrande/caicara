<div class="carousel-blog-recipes d-lg-none"> <?php
  $argsBlog = array(
    'post_type' => 'post',
    'order' => 'ASC',

  );
  $blog = new WP_Query($argsBlog);

  if ($blog->have_posts()) : while ($blog->have_posts()) :  $blog->the_post(); ?> <div class="item" style="background: url(<?= the_post_thumbnail_url(); ?>)center center no-repeat;"><div class="container"><div class="title"><?= the_title(); ?></div><p><?= the_excerpt(); ?></p><a href="<?= the_permalink(); ?>" class="btn-cta col-5 col-md-3 px-0">Leia mais</a></div></div> <?php endwhile;
  endif; ?> </div><div class="blog-posts white-bg d-none d-lg-block pb-lg-0"><div class="container"> <?php
    $argsBlogLg = array(
      'post_type' => 'post',
      'order' => 'ASC',
      'posts_per_page' => 4,


    );
    $counter = 0;
    $blog = new WP_Query($argsBlogLg);

    ?> <?php if ($blog->have_posts()) : while ($blog->have_posts()) :  $blog->the_post(); ?> <?php if ($counter == 0) : ?> <div class="col-12 px-0 d-flex"><div class="item col-lg-6 px-0" style="background: url(<?= the_post_thumbnail_url(); ?>)center center no-repeat;"><div class="container"><div class="text"><div class="title"><?= the_title(); ?></div><p><?= the_excerpt(); ?></p><a href="<?= the_permalink(); ?>" class="btn-cta col-5 px-0">Leia mais</a></div></div></div> <?php elseif ($counter == 1) : ?> <div class="col-lg-6 px-0"><div class="item medium-size" style="background: url(<?= the_post_thumbnail_url(); ?>)center center no-repeat;"><div class="container"><div class="text"><div class="title"><?= the_title(); ?></div><a href="<?= the_permalink(); ?>" class="btn-cta col-5 px-0">Leia mais</a></div></div></div><div class="d-lg-flex"> <?php elseif ($counter == 2) : ?> <div class="item low-size" style="background: url(<?= the_post_thumbnail_url(); ?>)center center no-repeat;"><div class="container"><div class="text"><div class="title"><?= the_title(); ?></div><a href="<?= the_permalink(); ?>" class="btn-cta col-8 px-0">Leia mais</a></div></div></div> <?php elseif ($counter == 3) : ?> <div class="item low-size" style="background: url(<?= the_post_thumbnail_url(); ?>)center center no-repeat;"><div class="container"><div class="text"><div class="title"><?= the_title(); ?></div><a href="<?= the_permalink(); ?>" class="btn-cta col-8 px-0">Leia mais</a></div></div></div></div></div></div> <?php $counter = -1;
            endif;
            $counter++;
          endwhile;
        endif;
    ?> </div></div>