<?php if (is_tax()) : $d_none = ' d-none';
  $d_block = ' d-block';
  $d_block_2 = ' d-none';
else : $d_block = ' d-none';
  $d_block_2 = ' d-none';
endif;

if (is_page('homepage')) : $d_none_home = 'd-none ';
endif;

if (is_page('exportacao')) : $d_block_english = 'd-block ';
  $d_none = 'd-none';
endif;


if (is_single()) : $d_none = ' d-none';
  $d_block = ' d-none';
  $d_block_2 = 'd-block';

endif;

// if (is_singular('receita')) : $d_block_2 = 'd-block';
//   $d_block = ' d-none';
// endif;


if (is_page('terceirizacao')) : $d_block = ' d-block';
  $d_none = ' d-none';
endif;

if (is_page(['locacao-e-vendas-de-maquinas', 'insumos-para-maquinas', 'terceirizacao', 'manutencao-e-oficina'])) : $d_block_2 = ' d-block';
  $d_none = ' d-none';
  $d_block = ' d-none';
endif;

?> <div class="intro-section <?= $d_none_home; ?> intro-nossas-marcas container pb-0 pb-lg-5"><span class="title <?= $d_none; ?>">Conheça<br>nossas marcas:</span> <span class="title title-english <?= $d_block_english; ?> ">Main lines of products</span> <span class="title <?= $d_block_2; ?>">Conheça nossas marcas:</span> <span class="title <?= $d_block; ?>">Conheça nossas outras marcas:</span></div><div class="wrapper-marcas container"><div class="carousel-marcas d-lg-flex justify-content-between"><a href="<?= get_site_url(); ?>/nossas_marcas/caicara/" class="item no-filter"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo-caicara.png" alt=""> </a><a href="<?= get_site_url(); ?>/nossas_marcas/sao_joaquim/" class="item"><img class="position-ajuste" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/saojoaquim.png" alt=""> </a><a href="<?= get_site_url(); ?>/nossas_marcas/san_pietro/" class="item"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/sanpietro.png" alt=""> </a><a href="<?= get_site_url(); ?>/nossas_marcas/itatibense/" class="item"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/itatibense.png" alt=""> </a><a href="<?= get_site_url(); ?>/nossas_marcas/bourbon/" class="item"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/bourbon.png" alt=""> </a><a href="<?= get_site_url(); ?>/nossas_marcas/negrao/" class="item"><img class="position-ajuste" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/negrao.png" alt=""> </a><a href="<?= get_site_url(); ?>/nossas_marcas/campo_verde/" class="item"><img class="position-ajuste" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/campoverde.png" alt=""></a></div><!-- <div class="control-lg-marca  d-none d-lg-block">

    <a class="next-marca">
      <i class='fa fa-chevron-right'></i>
    </a>
  </div> --></div>