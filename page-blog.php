<?php
$is_mobile = 'false';

$iphone = strpos($_SERVER['HTTP_USER_AGENT'], "iPhone");
$android = strpos($_SERVER['HTTP_USER_AGENT'], "Android");
$ipad = strpos($_SERVER['HTTP_USER_AGENT'], "iPad");
$berry = strpos($_SERVER['HTTP_USER_AGENT'], "BlackBerry");
$ipod = strpos($_SERVER['HTTP_USER_AGENT'], "iPod");

if ($iphone || $android || $ipad || $ipod || $berry == true) {
  $is_mobile = 'true';
}


get_header(); ?> <section class="banner-blog"><div class="banner"><h1><?= the_title(); ?></h1></div></section><section class="intro-section container"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/blog-icon.png" alt=""> <span class="title">Para ler tomando um cafézinho</span><div class="line"></div></section><section class="destaque-blog blog-recipes"><span class="sub container">destaques</span><div class="wrapper"><div class="carousel-blog-recipes"> <?php
      $argsBlog = array(
        'post_type' => 'post',
        'order' => 'ASC',
        'meta_query' => array(
          array(
            'key' => 'destaque',
            'value' => '1',
            'compare' => '=='
          )
        )

      );
      $blog = new WP_Query($argsBlog);

      if ($blog->have_posts()) : while ($blog->have_posts()) :  $blog->the_post(); ?> <div class="item" style="background: url(<?= the_post_thumbnail_url(); ?>)center center no-repeat;"><div class="container"><div class="title col-lg-4 px-0"><?= the_title(); ?></div><p class=""><?= the_excerpt(); ?></p><a href="<?= the_permalink(); ?>" class="btn-cta col-lg-2 col-5 px-0">Leia mais</a></div></div> <?php endwhile;
      endif; ?> </div></div></section><section class="blog-posts"><div class="container d-lg-none"> <?php
    $argsBlog = array(
      'post_type' => 'post',
      'order' => 'ASC',
      'posts_per_page' => 2,

    );
    $counter = 0;
    $blog = new WP_Query($argsBlog);

    if ($blog->have_posts()) : while ($blog->have_posts()) :  $blog->the_post(); ?> <?php if ($counter < 1) : ?> <div class="item" style="background: url(<?= the_post_thumbnail_url(); ?>)center center no-repeat;"><div class="container"><div class="text"><div class="title"><?= the_title(); ?></div><p><?= the_excerpt(); ?></p><a href="<?= the_permalink(); ?>" class="btn-cta col-5 px-0">Leia mais</a></div></div></div> <?php $counter++; ?> <div class="item low-size" style="background: url(<?= the_post_thumbnail_url(); ?>)center center no-repeat;"><div class="container"><div class="text"><div class="title"><?= the_title(); ?></div><a href="<?= the_permalink(); ?>" class="btn-cta col-5 px-0">Leia mais</a></div></div></div> <?php
        endif;
      endwhile;
    endif; ?> </div><div class="container d-none d-lg-flex flex-wrap"> <?php

    $numberOfPosts = 8;

    $argsBlogLg = array(
      'post_type' => 'post',
      'order' => 'ASC',
      'posts_per_page' => $numberOfPosts,
    );

    $blog = new WP_Query($argsBlogLg);

    ?> <?php if ($blog->have_posts()) : while ($blog->have_posts()) :  $blog->the_post(); ?> <?php

        if (wp_count_posts()->publish % 2 !== 0) {
          $col = ' impar ';
        };

        if (wp_count_posts()->publish < $numberOfPosts) {
          $btn_none = ' d-none';
        };

        ?> <div class="item col-lg-6 <?= $col; ?> px-0" style="background: url(<?= the_post_thumbnail_url(); ?>)center center no-repeat;"><div class="container"><div class="text"><div class="title"><?= the_title(); ?></div><p><?= the_excerpt(); ?></p><a href="<?= the_permalink(); ?>" class="btn-cta col-5 px-0">Leia mais</a></div></div></div> <?php
      endwhile;
    endif;
    ?> </div><a href="<?= $numberOfPosts = $numberOfPosts + 8; ?>" id="showMoreBlog" data-responsive="<?= $is_mobile; ?>" class="btn-cta <?= $btn_none; ?> col-4 col-lg-1"><span>Ver mais</span> <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/teste.gif" alt=""></a></section><section class="blog-recipes recipes-blog"><div class="header"><div class="d-flex align-items-center justify-content-center"><img class="mr-3" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/cafe-icon.png" alt=""> <span class="title">Receitas</span></div><div class="line col-5 px-0"></div><p class="sub">Café deixa tudo mais gostoso</p></div> <?= get_template_part('carousel-receitas'); ?> </section> <?= get_footer(); ?>