<?php

the_post();
$paged = (isset($_GET['pagina']) ? $_GET['pagina'] : 1);

$terms = get_the_terms($post->ID, 'nossas_marcas');

if ($terms[0]->slug == 'caicara') : $tax_color = ' tax-red';
elseif ($terms[0]->slug == 'sao_joaquim') : $tax_color = ' tax-yellow';
elseif ($terms[0]->slug == 'san_pietro') : $tax_color = ' tax-brown';
elseif ($terms[0]->slug == 'itatibense') : $tax_color = ' tax-dbrown';
elseif ($terms[0]->slug == 'bourbon') : $tax_color = ' tax-black';
elseif ($terms[0]->slug == 'negrao') : $tax_color = ' tax-wine';
elseif ($terms[0]->slug == 'campo_verde') : $tax_color = ' tax-green';
endif;


$post_ID = $post->ID;

$new_limit = 6;

$iphone = strpos($_SERVER['HTTP_USER_AGENT'], "iPhone");
$android = strpos($_SERVER['HTTP_USER_AGENT'], "Android");
$ipad = strpos($_SERVER['HTTP_USER_AGENT'], "iPad");
$berry = strpos($_SERVER['HTTP_USER_AGENT'], "BlackBerry");
$ipod = strpos($_SERVER['HTTP_USER_AGENT'], "iPod");

if ($iphone || $android || $ipad || $ipod || $berry == true) {
  $new_limit = 4;
}

get_header();

?> <section class="banner-nossas-marcas-single"><div class="banner" style="background: url(<?= the_field('banner_marca', $terms[0]); ?>) center center no-repeat;"></div></section><section class="intro-section intro-section-tax mb-lg-5 container"><div class="box-marca-img col-lg-3 px-5"><img src="<?= the_field('logo_categoria', $terms[0]); ?>" alt=""></div></section><section class="product-info mb-5"><div class="container intro-section-tax d-lg-block <?= $tax_color; ?>"><span class="title col-lg-7 px-0"><?= the_title(); ?></span> <?php if (get_field('embalagem')) :; ?> <span class="sub-title">Embalagem: <?= the_field('embalagem'); ?> <?= the_field('peso'); ?></span> <?php endif; ?> <div class="line col-12 col-lg-5 p-0"></div><p class="description-intro d-none d-lg-block"><?= the_field('descricao_intro'); ?></p><div class="d-lg-flex justify-content-lg-between"><div class="col-lg-6 px-0"><img class="img-produto d-lg-none" src="<?= get_the_post_thumbnail_url(); ?>" alt=""><p class="description-intro d-lg-none"><?= the_field('descricao_intro'); ?></p><div class="d-lg-flex box-items"><div class="box-description"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/icon-dimensao.png" alt=""><div class="text"><span><b>Dimensões do produto (CxLxA)</b></span> <span><?php if (get_field('dimensoes_do_produto')) : echo the_field('dimensoes_do_produto');
                    else : echo '-';
                    endif; ?></span></div></div><div class="box-description"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/icon-dimensao.png" alt=""><div class="text"><span><b>Dimensões do fardo (CxLxA)</b></span> <span><?php if (get_field('dimensoes_do_fardo')) : echo the_field('dimensoes_do_fardo');
                    else : echo '-';
                    endif; ?></span></div></div></div><div class="d-lg-flex box-items"><div class="box-description"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/icon-peso.png" alt=""><div class="text"><span><b>Peso bruto</b></span> <span><?php if (get_field('peso_bruto')) : echo  the_field('peso_bruto');
                    else : echo '-';
                    endif; ?></span></div></div><div class="box-description"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/icon-validade.png" alt=""><div class="text"><span><b>Validade</b></span> <span><?php if (get_field('validade')) : echo  the_field('validade');
                    else : echo '-';
                    endif; ?></span></div></div></div><div class="box-description"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/icon-tipo.png" alt=""><div class="text"><span><b>Tipo de café</b></span> <span><?php if (get_field('tipo_do_cafe')) : echo  the_field('tipo_do_cafe');
                  else : echo '-';
                  endif; ?></span></div></div><div class="d-lg-flex box-items"><div class="box-description"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/icon-bebida.png" alt=""><div class="text"><span><b>Bebida</b></span> <span><?php if (get_field('bebida')) : echo  the_field('bebida');
                    else : echo '-';
                    endif; ?></span></div></div><div class="box-description"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/icon-torracao.png" alt=""><div class="text"><span><b>Torração</b></span> <span><?php if (get_field('torracao')) : echo  the_field('torracao');
                    else : echo '-';
                    endif; ?></span></div></div></div><div class="qualidade-certificada"><span class="title col-5 col-lg-3 px-0">Qualidade certificada</span><div class=""><img class="col-5 col-lg-4 px-0" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/selo-abic.png" alt=""> <img class="col-5 col-lg-4 px-0" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/selo-abic-2.png" alt=""></div></div></div><img class="img-produto col-lg-5 px-0 d-lg-block d-none" src=" <?= get_the_post_thumbnail_url(); ?>" alt=""></div> <?php if (get_field('como_preparar')) : ?> <div class="como-preparar"><span class="title col-lg-7 px-0">Como preparar</span><p><?= the_field('como_preparar'); ?></p></div> <?php endif; ?> <?php if (get_field('informacoes_adicionais')) : ?> <div class="informacoes-adicionais pt-5"><span class="title col-lg-7 px-0">Informações adicionais</span><p><?= the_field('informacoes_adicionais'); ?></p></div> <?php endif; ?> </div></section><section class="nossos-produtos custom-nossos"><div class="container"><span class="title col-lg-7">Confira nossos outros produtos <?= $terms[0]->name; ?></span><div class="line col-7 col-lg-2 px-0"></div><div class="product-box pt-0 col-lg-12"> <?php
      $argsProdutos =   array(
        'post_type' => 'produtos',
        'post__not_in' => array($post_ID),
        'order' => 'ASC',
        'posts_per_page' => $new_limit,
        'paged' => $paged,
        'tax_query' => array(
          array(
            'taxonomy' => 'nossas_marcas',
            'field' => 'term_id',
            'terms' => $terms[0]->term_id,
          )

        )
      );

      $products = new WP_Query($argsProdutos);


      while ($products->have_posts()) : $products->the_post();


      ?> <a href="<?= the_permalink(); ?>" class="item"><img src="<?= get_the_post_thumbnail_url(); ?>" alt=""><div class="line-red col-5 px-0"></div><span class="name"><?= the_title(); ?></span> <?php if (get_field('embalagem')) :; ?> <span class="name">Embalagem <?= the_field('embalagem'); ?></span> <?php endif; ?> </a> <?php endwhile; ?> </div><!-- <a href="#" id="loadMoreProducts" data-marca="<?= $terms[0]->term_id; ?>" data-post-id="<?= $post_ID; ?>" class="btn-cta col-5 col-lg-2">
      <span>Ver mais</span>
      <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/teste.gif" alt="">
    </a> --><div class="barradenavegacao <?= $tax_color; ?>"> <?php


      echo paginate_links(array(
        'format' =>
        '?pagina=%#%#anchor', 'show_all' => false, 'current' => max(1, $paged), 'total' => $products->max_num_pages, 'prev_text' => '<i class="fas fa-caret-left fa-2x"></i>', 'next_text' => '<i class="fas fa-caret-right fa-2x"></i>',
        'type' => 'list'
      ));
      ?> </div></div></section> <?php echo get_template_part('onde-estamos', '', array(
  'produto' => $terms[0],
)); ?> <?= get_template_part('componente-revenda', '', array(
  'produto' => $terms[0],
)); ?> <?= get_template_part('nossas-marcas'); ?> <?= get_footer(); ?>