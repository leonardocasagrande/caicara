<?= get_header(); $numerowhats = get_field('atendimento_whats', 41);?> <section class="banner-insumos"><div class="banner"><h1><?= the_title(); ?></h1></div></section><section class="intro-section is-insumos container"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/icon-insumos.png" alt=""> <span class="title col-lg-5 pt-4 px-0"><?= get_field('titulo_intro') ?></span><div class="line"></div> <?= get_field('texto_intro') ?> </section><section class="insumos"> <?php

  $taxonomies = get_terms(array(
    'taxonomy' => 'categoria_insumo',
    'hide_empty' => false,
  ));

  ?> <?php foreach ($taxonomies as $taxonomy) : ?> <a class="btn-categoria" data-bs-toggle="collapse" href="#collapseExample-<?= $taxonomy->term_id; ?>" role="button" aria-expanded="false" aria-controls="collapseExample"><div class="green col-3 col-lg-1"><img src="<?= the_field('imagem_cat_insumo', $taxonomy); ?>" alt=""></div><div class="red"><span class="title"><?= $taxonomy->name ?></span><i class="fas fa-plus"></i> <i class="fas fa-minus"></i></div></a><div class="collapse" id="collapseExample-<?= $taxonomy->term_id; ?>"><div class="card card-body"><div class="item-box"> <?php

          $argsInsumos =   array(
            'post_type' => 'insumos',
            'order' => 'ASC',
            'tax_query' => array(
              array(
                'taxonomy' => 'categoria_insumo',
                'field' => 'term_id',
                'terms' => $taxonomy->term_id,
              )
            )
          );

          $catquery = new WP_Query($argsInsumos); ?> <?php while ($catquery->have_posts()) : $catquery->the_post();
          ?> <div class="item"><div class="img-header" style="background: url(<?= the_post_thumbnail_url(); ?>)"></div><div class="content"><span class="title"><?= the_title(); ?></span><span class="unidade"><?= the_field('unidade_insumo'); ?></span><p><?= the_content(); ?></p><a href="https://api.whatsapp.com/send?phone=55<?= $numerowhats ?>&text=Ol%C3%A1%2C%20gostaria%20de%20saber%20mais%20sobre%20o%20<?= $post->post_title; ?>" target="_blank" class="btn-cta col-8 px-0">Como comprar</a></div></div> <?php endwhile;
          ?> </div></div></div> <?php endforeach; ?> </section><section class="btn-referencia pt-5 container"><span class="title col-lg-8 px-0">Gostaria de comprar ou alugar uma máquina?</span> <a href="<?= get_site_url(); ?>/locacao-e-vendas-de-maquinas-de-cafe" class="btn-cta col-lg-2 col-8">Clique aqui</a></section> <?= get_template_part('nossas-marcas'); ?> <?= get_footer(); ?>