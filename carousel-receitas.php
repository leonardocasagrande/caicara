<div class="wrapper-receitas container"><div class="carousel-destaques-receitas"> <?php
    $argsReceitas = array(
      'post_type' => 'receita',
      'orderby' => 'rand',
      'posts_per_page' => 4,
    );

    $receitas = new WP_Query($argsReceitas);

    if ($receitas->have_posts()) : while ($receitas->have_posts()) :  $receitas->the_post();

        $taxonomiaReceita = get_the_terms(get_the_id(), 'categoria_receita');

    ?> <div class="item"><div class="img-header" style="background: url(<?= the_post_thumbnail_url(); ?>)"></div><div class="content"><span class="categoria">/ <?php foreach ($taxonomiaReceita as $taxonomia) : echo $taxonomia->name;
              endforeach; ?> </span><span class="title"><?= the_title(); ?></span><a href="<?= the_permalink(); ?>" class="btn-cta col-lg-6 col-7 px-0">Confira a receita</a></div></div> <?php
      endwhile;
    endif; ?> </div></div>