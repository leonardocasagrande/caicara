<?= get_header(); ?> <section class="banner-contato"><div class="banner"><h1><?= the_title(); ?></h1></div></section><section class="intro-section intro-section-contato container"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/icon-contato.png" alt=""> <span class="title title-contato">Nossa paixão por café é o que nos aproxima</span><div class="line"></div><p class="pt-4 pt-lg-0">Será um prazer te atender!<br>Preencha o formulário abaixo que entraremos em contato:</p></section><section class="section-form"><div class="container col-lg-8"><div class="form-contato"> <?php echo do_shortcode('[contact-form-7 id="60" title="formulario-contato"]'); ?> <!-- <input type="text" name="" placeholder="Nome Completo" id="">

      <div class="d-lg-flex justify-content-between">
        <input class="custom-email" type="email" name="E-mail" placeholder="E-mail" id="">

        <input class="custom-tel" type="text" name="" placeholder="Telefone" id="">
      </div>

      <div class="d-lg-flex justify-content-between">
        <select name="estados-brasil" class="custom-estado">
          <option value="" disabled selected>Estado</option>
          <option value="AC">Acre</option>
          <option value="AL">Alagoas</option>
          <option value="AP">Amapá</option>
          <option value="AM">Amazonas</option>
          <option value="BA">Bahia</option>
          <option value="CE">Ceará</option>
          <option value="DF">Distrito Federal</option>
          <option value="ES">Espírito Santo</option>
          <option value="GO">Goiás</option>
          <option value="MA">Maranhão</option>
          <option value="MT">Mato Grosso</option>
          <option value="MS">Mato Grosso do Sul</option>
          <option value="MG">Minas Gerais</option>
          <option value="PA">Pará</option>
          <option value="PB">Paraíba</option>
          <option value="PR">Paraná</option>
          <option value="PE">Pernambuco</option>
          <option value="PI">Piauí</option>
          <option value="RJ">Rio de Janeiro</option>
          <option value="RN">Rio Grande do Norte</option>
          <option value="RS">Rio Grande do Sul</option>
          <option value="RO">Rondônia</option>
          <option value="RR">Roraima</option>
          <option value="SC">Santa Catarina</option>
          <option value="SP">São Paulo</option>
          <option value="SE">Sergipe</option>
          <option value="TO">Tocantins</option>
        </select>

        <input class="custom-cidade" type="text" name="" placeholder="Cidade" id="">

        <select class="custom-departamento" name="" id="">
          <option value="" selected disabled>Departamento</option>
        </select>
      </div>

      <select name="" id="">
        <option value="" selected disabled>Assunto</option>
      </select>

      <textarea name="" placeholder="Mensagem" id="" cols="30" rows="10"></textarea>

      <input type="file" name="" placeholder="Enviar Documento" id="">

      <input type="submit" class="btn-cta col-4 col-lg-2 px-0" value="enviar"> --></div><div class="infos d-lg-flex justify-content-between pt-lg-5 mt-lg-5"><div><div class="box"><span class="mini-title">Exportação</span> <?php

          // Check rows exists.
          if (have_rows('exportacao')) :

            // Loop through rows.
            while (have_rows('exportacao')) : the_row();
              $numero = get_sub_field('numero');
              if ($numero) :
          ?> <a href="tel:+55<?= $numero; ?>" class="text"><i class="fas fa-phone-alt"></i><?php echo formatarNumeroCelular($numero); ?></a> <?php else : endif;
            endwhile;
          endif; ?> <?php

          // Check rows exists.
          if (have_rows('e_mail_exportacao')) :

            // Loop through rows.
            while (have_rows('e_mail_exportacao')) : the_row();
              $email = get_sub_field('e_mail');
              if ($email) :
          ?> <a href="mailto:<? echo ($email) ?>" class="text"><i class="fas fa-envelope-open"></i><? echo ($email) ?></a> <?php else : endif;
            endwhile;
          endif; ?> </div><div class="box"><span class="mini-title">Comercial e terceirização</span> <?php

          // Check rows exists.
          if (have_rows('comercial_e_terceirizacao')) :

            // Loop through rows.
            while (have_rows('comercial_e_terceirizacao')) : the_row();
              $numero = get_sub_field('numero');
              if ($numero) :
          ?> <a href="tel:+55<?= $numero ?>" class="text"><i class="fas fa-phone-alt"></i><?php echo formatarNumeroCelular($numero) ?></a> <?php else : endif;
            endwhile;
          endif; ?> <?php

          // Check rows exists.
          if (have_rows('comercial_e_terceirizacao_email')) :

            // Loop through rows.
            while (have_rows('comercial_e_terceirizacao_email')) : the_row();
              $e_mail = get_sub_field('e_mail');
              if ($e_mail) :
          ?> <a href="mailto:<? echo ($e_mail) ?>" class="text"><i class="fas fa-envelope-open"></i><? echo ($e_mail) ?></a> <?php else : endif;
            endwhile;
          endif; ?> </div><div class="box"><span class="mini-title">Ouvidoria - Sac</span> <?php

          // Check rows exists.
          if (have_rows('ouvidoria__sac')) :

            // Loop through rows.
            while (have_rows('ouvidoria__sac')) : the_row();
              $numero = get_sub_field('numero');
              if ($numero) :
          ?> <a href="tel:+55<?= $numero ?>" class="text"><i class="fas fa-phone-alt"></i><?php echo formatarNumeroCelular($numero) ?></a> <?php else : endif;
            endwhile;
          endif; ?> <?php

// Check rows exists.
if (have_rows('ouvidoria__sac_email')) :

  // Loop through rows.
  while (have_rows('ouvidoria__sac_email')) : the_row();
    $e_mail = get_sub_field('e_mail');
    if ($e_mail) :
?> <a href="mailto:<? echo ($e_mail) ?>" class="text"><i class="fas fa-envelope-open"></i><? echo ($e_mail) ?></a> <?php else: endif; endwhile; endif; ?> </div></div><div><div class="box"><span class="mini-title">Televendas</span> <?php

          // Check rows exists.
          if (have_rows('televendas')) :

            // Loop through rows.
            while (have_rows('televendas')) : the_row();
              $numero = get_sub_field('numero');
              if ($numero) :
          ?> <a href="tel:+55<?= $numero ?>" class="text"><i class="fas fa-phone-alt"></i><?php echo formatarNumeroCelular($numero) ?></a> <?php else : endif;
            endwhile;
          endif; ?> <?php

          // Check rows exists.
          if (have_rows('televendas_email')) :

            // Loop through rows.
            while (have_rows('televendas_email')) : the_row();
              $e_mail = get_sub_field('e_mail');
              if ($e_mail) :
          ?> <a href="mailto:<? echo ($e_mail) ?>" class="text"><i class="fas fa-envelope-open"></i><? echo ($e_mail) ?></a> <?php else : endif;
            endwhile;
          endif; ?> </div><div class="box"><span class="mini-title">Trabalhe conosco / RH</span> <?php

          // Check rows exists.
          if (have_rows('trabalhe_conosco__rh')) :

            // Loop through rows.
            while (have_rows('trabalhe_conosco__rh')) : the_row();
              $numero = get_sub_field('numero');
              if ($numero) :
          ?> <a href="tel:+55<?= $numero ?>" class="text"><i class="fas fa-phone-alt"></i><?php echo formatarNumeroCelular($numero) ?></a> <?php else : endif;
            endwhile;
          endif; ?> <?php

          // Check rows exists.
          if (have_rows('trabalhe_conosco__rh_email')) :

            // Loop through rows.
            while (have_rows('trabalhe_conosco__rh_email')) : the_row();
              $e_mail = get_sub_field('e_mail');
              if ($e_mail) :
          ?> <a href="mailto:<? echo ($e_mail) ?>" class="text"><i class="fas fa-envelope-open"></i><? echo ($e_mail) ?></a> <?php else : endif;
            endwhile;
          endif; ?> </div><div class="box"><span class="mini-title lh-custom">Vendas e locação de Máquinas / Manutenção </span> <?php

          // Check rows exists.
          if (have_rows('vendas_e_locacao_de_maquinas__manutencao')) :

            // Loop through rows.
            while (have_rows('vendas_e_locacao_de_maquinas__manutencao')) : the_row();
              $numero = get_sub_field('numero');
              if ($numero) :
          ?> <a href="tel:+55<?= $numero ?>" class="text"><i class="fas fa-phone-alt"></i><?php echo formatarNumeroCelular($numero) ?></a> <?php else : endif;
            endwhile;
          endif; ?> <?php

          // Check rows exists.
          if (have_rows('vendas_e_locacao_de_maquinas__manutencao_email')) :

            // Loop through rows.
            while (have_rows('vendas_e_locacao_de_maquinas__manutencao_email')) : the_row();
              $e_mail = get_sub_field('e_mail');
              if ($e_mail) :
          ?> <a href="mailto:<? echo ($e_mail) ?>" class="text"><i class="fas fa-envelope-open"></i><? echo ($e_mail) ?></a> <?php else : endif;
            endwhile;
          endif; ?> </div></div></div></div></section><div class="container"> <?= get_template_part('onde-estamos'); ?> </div> <?= get_template_part('infos-red'); ?> <?= get_footer(); ?>